package frc.lib.mathematics

import org.ghrobotics.lib.mathematics.lerp
import org.junit.Assert.*
import org.junit.Test

class InterpolatableTester(val value: Double): Interpolatable<InterpolatableTester> {
    override fun interpolate(other: InterpolatableTester, alpha: Double): InterpolatableTester {
        return InterpolatableTester(value.lerp(other.value, alpha))
    }

}

class InterpolatingLookupTableTest{
    @Test
    fun testInterpolatingLookupTable() {
        val test1 = InterpolatableTester(1.0)
        val test2 = InterpolatableTester(2.0)
        val lookupTable = InterpolatingLookupTable<InterpolatableTester>()
        assertNull(lookupTable.get(0.0))

        lookupTable.put(0.0, test1)

        assertEquals(lookupTable.get(0.0), test1)
        assertEquals(lookupTable.get(1.0), test1)
        assertEquals(lookupTable.get(-1.0), test1)

        lookupTable.put(1.0, test2)

        assertEquals(lookupTable.get(0.0), test1)
        assertEquals(lookupTable.get(1.0), test2)

        assertEquals(lookupTable.get(0.5)?.value ?: 123.0, 1.5, 1E-6)
    }

    @Test
    fun testInterpolatingLookupFrom() {
        val table = interpolatingLookupFrom(
                0.0 to InterpolatableTester(0.0),
                1.0 to InterpolatableTester(1.0),
                2.0 to InterpolatableTester(3.0)
        )

        assertEquals(0.5, table.get(0.5)?.value ?: 123.0, 1E-6)
        assertEquals(1.0, table.get(1.0)?.value ?: 123.0, 1E-6)
        assertEquals(2.0, table.get(1.5)?.value ?: 123.0, 1E-6)
        assertEquals(3.0, table.get(2.0)?.value ?: 123.0, 1E-6)
        assertEquals(3.0, table.get(3.0)?.value ?: 123.0, 1E-6)
    }
}