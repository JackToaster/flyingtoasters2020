package frc.robot.subsystems

import edu.wpi.first.wpilibj.geometry.Pose2d
import edu.wpi.first.wpilibj.geometry.Rotation2d
import edu.wpi.first.wpilibj.geometry.Translation2d
import frc.robot.Config.Launcher.maxInnerPortAngle
import frc.robot.Config.Launcher.maxInnerPortDistance
import frc.robot.Config.Launcher.outerToInnerPortDistanceM
import frc.robot.Config.Launcher.setpoints
import org.ghrobotics.lib.mathematics.units.derived.degrees
import org.ghrobotics.lib.mathematics.units.derived.inDegrees
import org.ghrobotics.lib.mathematics.units.derived.velocity
import org.ghrobotics.lib.mathematics.units.inMeters
import org.ghrobotics.lib.mathematics.units.seconds
import org.junit.Test

import org.junit.Assert.*
import org.opencv.core.RotatedRect
import kotlin.math.sqrt

class VisionKtTest {
    @Test
    fun correctVisionPoseForLatency() {
        val angVel = 90.0.degrees.velocity
        val time = 1.0.seconds
        val offset = Pose2d(Translation2d(1.0, 0.0), Rotation2d(0.0))

        val corrected = frc.robot.subsystems.correctVisionPoseForLatency(angVel, time, offset)

        assertEquals(0.0, corrected.translation.x, 1E-6)
        assertEquals(-1.0, corrected.translation.y, 1E-6)
        assertEquals(-90.0, corrected.rotation.degrees, 1E-6)
    }

    @Test
    fun getSetpointFromTargetingData() {
        val firstSetpoint = setpoints.firstEntry()

        val launcherTarget = frc.robot.subsystems.getSetpointFromTargetingData(TargetingData(firstSetpoint.key, Rotation2d(0.0)))

        assertEquals(firstSetpoint.value.flywheelVelocity, launcherTarget?.flywheelVelocity ?: 1234567.0, 1E-6)
        assertEquals(firstSetpoint.value.hoodPosition, launcherTarget?.hoodPosition ?: 12345678.0, 1E-6)
        assertEquals(firstSetpoint.value.turretOffsetDeg, launcherTarget?.turretAngle?.degrees ?: 12345678.0, 1E-6)
    }

    @Test
    fun visionCoordsToFieldCoords() {
        val visionCoords = Pose2d(Translation2d(1.0, 1.0), Rotation2d(0.0))
        val robotPose = Pose2d(Translation2d(10.0, 10.0), Rotation2d(0.0))
        val turretAngle = Rotation2d.fromDegrees(90.0)
        val fieldCoords = visionCoordsToFieldCoords(visionCoords, robotPose, turretAngle)

        assertEquals(9.0, fieldCoords.translation.x, 1E-6)
        assertEquals(11.0, fieldCoords.translation.y, 1E-6)
        assertEquals(90.0, fieldCoords.rotation.degrees, 1E-6)
    }

    @Test
    fun getSelectedTargetingDataInnerPort() {
        val xDistance = maxInnerPortDistance.inMeters() - 1.0
        val targetPose = Pose2d(Translation2d(xDistance,0.0), Rotation2d(0.0))
        val robotPose = Pose2d(Translation2d(0.0,0.0), Rotation2d(0.0))
        val targetingData = getSelectedTargetingData(targetPose, robotPose)

        assertEquals(0.0, targetingData.fieldRelativeAngle.degrees, 1E-6)
        assertEquals(xDistance + outerToInnerPortDistanceM, targetingData.distance, 1E-6)
    }

    @Test
    fun getSelectedTargetingDataOuterPort() {
        val xDistance = maxInnerPortDistance.inMeters() + 1.0
        val targetPose = Pose2d(Translation2d(xDistance,0.0), Rotation2d(0.0))
        val robotPose = Pose2d(Translation2d(0.0,0.0), Rotation2d(0.0))
        val targetingData = getSelectedTargetingData(targetPose, robotPose)

        assertEquals(0.0, targetingData.fieldRelativeAngle.degrees, 1E-6)
        assertEquals(xDistance, targetingData.distance, 1E-6)
    }

    @Test
    fun getSelectedTargetingDataAngled() {
        val xDistance = maxInnerPortDistance.inMeters() - 1.0
        val angle = maxInnerPortAngle + 1.0.degrees
        val targetPose = Pose2d(Translation2d(xDistance,0.0), Rotation2d.fromDegrees(angle.inDegrees()))
        val robotPose = Pose2d(Translation2d(0.0,0.0), Rotation2d(0.0))
        val targetingData = getSelectedTargetingData(targetPose, robotPose)

        assertEquals(0.0, targetingData.fieldRelativeAngle.degrees, 1E-6)
        assertEquals(xDistance, targetingData.distance, 1E-6)
    }

    @Test
    fun getSelectedTargetingDataRobotAngled() {
        val xDistance = maxInnerPortDistance.inMeters() - 1.0
        val angle = maxInnerPortAngle + 1.0.degrees
        val targetPose = Pose2d(Translation2d(xDistance,0.0), Rotation2d())
        val robotPose = Pose2d(Translation2d(0.0,0.0), Rotation2d.fromDegrees(angle.inDegrees()))
        val targetingData = getSelectedTargetingData(targetPose, robotPose)

        assertEquals(0.0, targetingData.fieldRelativeAngle.degrees, 1E-6)
        assertEquals(xDistance + outerToInnerPortDistanceM, targetingData.distance, 1E-6)
    }

    @Test
    fun getSelectedTargetingDataWeirdAngle() {
        val yDistance = maxInnerPortDistance.inMeters() / 2.0
        val botAngle = Rotation2d.fromDegrees(90.0)
        val targetPose = Pose2d(Translation2d(yDistance,yDistance), Rotation2d.fromDegrees(45.0))
        val robotPose = Pose2d(Translation2d(0.0,0.0), botAngle)
        val targetingData = getSelectedTargetingData(targetPose, robotPose)

        assertEquals(45.0, targetingData.fieldRelativeAngle.degrees, 1E-6)
        assertEquals(yDistance * sqrt(2.0) + outerToInnerPortDistanceM, targetingData.distance, 1E-6)
    }
}