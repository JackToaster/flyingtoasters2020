/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot

import edu.wpi.first.wpilibj.Sendable
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.Command
import edu.wpi.first.wpilibj2.command.InstantCommand
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup
import frc.robot.autonomous.TrajectoryTestAuton
import frc.robot.autonomous.VelocityTestAuton
import frc.robot.autonomous.modes.*
import frc.robot.controls.ControlMapping
import frc.robot.subsystems.BallFeeder
import frc.robot.subsystems.Climber
import frc.robot.subsystems.Vision
import frc.robot.subsystems.drive.Drive
import frc.robot.subsystems.launcher.Flywheel
import frc.robot.subsystems.launcher.Hood
import frc.robot.subsystems.launcher.Turret


/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
class RobotContainer {
  // The robot's subsystems and commands are defined here...
  //val m_autoCommand: ExampleCommand
  val drive = Drive
  val hood = Hood
  val flywheel = Flywheel
  val feeder = BallFeeder
  val vision = Vision
  val turret = Turret
  val controlMapping = ControlMapping
  val climber = Climber
  //val musicPlayer = MusicPlayer // RIP


  val autoCommandChooser: SendableChooser<() -> Command> = SendableChooser()
  val delayWidget = Shuffleboard.getTab("Auton").addPersistent("Delay time", 0.0)
  val pushPartnerWidget = Shuffleboard.getTab("Auton").addPersistent("Push Partner", false)

  fun getDelay() = delayWidget.entry.getDouble(0.0)
  fun getPush() = pushPartnerWidget.entry.getBoolean(false)

  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  init {
    pushPartnerWidget.entry.setBoolean(false)
    autoCommandChooser.setDefaultOption("None") {InstantCommand()}
    mapOf(
            "3 ball from center" to { ShootOnlyAuto(getDelay()) },
            "5 ball center to right rendezvous" to { RightRendezvousAuto() },
            "Own trench - 6 ball" to { OwnTrenchAuto() },
            "Own trench - 8 ball" to { OwnTrenchAuto8() },
            "Own trench & rendezvous - 10 ball" to { TenLemonAuto() },
            "Opponent trench - 5 ball" to { OpponentTrenchAuto() }
    ).forEach{autoCommandChooser.addOption(it.key, it.value)}

    Shuffleboard.getTab("Auton").add(autoCommandChooser)
  }


  fun getAutonomousCommand(): Command {
    if(getPush()) {
      return SequentialCommandGroup(PushPartnerCommand(), autoCommandChooser.getSelected()())
    }else{
      return autoCommandChooser.getSelected()()
    }
  }
}
