package frc.robot.subsystems

import edu.wpi.first.wpilibj.DigitalOutput
import edu.wpi.first.wpilibj.Timer
import edu.wpi.first.wpilibj2.command.*
import frc.lib.commands.BlockCommand
import frc.robot.Config.Launcher.Vision
import org.ghrobotics.lib.mathematics.units.SIUnit
import org.ghrobotics.lib.mathematics.units.Second
import org.ghrobotics.lib.mathematics.units.inSeconds
import org.ghrobotics.lib.mathematics.units.seconds

object VisionLEDs: SubsystemBase() {
    private val dio = DigitalOutput(Vision.ledDioPin)
    init {
        dio.enablePWM(0.0)
    }

    fun set(on: Boolean) = dio.updateDutyCycle(if(on) Vision.brightness else 0.0)
}


class  LedOnCommand: BlockCommand({VisionLEDs.set(true)}, VisionLEDs)

class LedOffCommand: BlockCommand({VisionLEDs.set(false)}, VisionLEDs)

class LedStrobeCommand(val period: SIUnit<Second> = 0.25.seconds): CommandBase() {
    init {
        addRequirements(VisionLEDs)
    }

    override fun execute() {
        val time = Timer.getFPGATimestamp()
        if(time % period.inSeconds() < period.inSeconds() / 2.0) VisionLEDs.set(true)
        else VisionLEDs.set(false)
    }

    override fun end(interrupted: Boolean) {
        VisionLEDs.set(false)
    }
}