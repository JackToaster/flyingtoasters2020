package frc.robot.subsystems.launcher

import com.ctre.phoenix.motorcontrol.ControlMode
import com.ctre.phoenix.motorcontrol.DemandType
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.CommandBase
import edu.wpi.first.wpilibj2.command.SubsystemBase
import frc.lib.commands.BlockCommand
import frc.robot.Config
import kotlin.math.abs

object Flywheel: SubsystemBase() {
    private val motor = Config.Launcher.Flywheel.flywheelMaster

    fun setFlywheel(velocityMetersPerSecond: Double){
        val feedForwardVolts = velocityMetersPerSecond * Config.Launcher.Flywheel.kV

        val velocityMotorUnits = velocityMetersPerSecond / Config.Launcher.Flywheel.encoderRateToVelocity.value

        motor.set(ControlMode.Velocity, velocityMotorUnits,
                DemandType.ArbitraryFeedForward, feedForwardVolts / 12.0)
    }

    override fun periodic() {
        if(motor.motorOutputVoltage != 0.0) {
            val kV = getVelocity() / motor.motorOutputVoltage
            SmartDashboard.putNumber("Flywheel KV", kV)
        }

        SmartDashboard.putNumber("Follower voltage", Config.Launcher.Flywheel.flywheelFollower.motorOutputVoltage)
        SmartDashboard.putNumber("Master voltage", Config.Launcher.Flywheel.flywheelMaster.motorOutputVoltage)

    }

    fun flywheelOff() {
        motor.set(ControlMode.PercentOutput, 0.0)
    }

    fun getVelocity() = motor.selectedSensorVelocity * Config.Launcher.Flywheel.encoderRateToVelocity.value
}

class FlywheelSpinUpCommand(val velocityMetersPerSecond: () -> Double,
                            val tolerance: Double = Config.Launcher.Flywheel.velocityTolerance): CommandBase(){
    init {
        addRequirements(Flywheel)
    }

    override fun initialize() {
        Flywheel.setFlywheel(velocityMetersPerSecond())
    }

    override fun execute() {
        SmartDashboard.putNumber("Flywheel Velocity", Flywheel.getVelocity())
    }

    override fun isFinished(): Boolean {
        return abs(Flywheel.getVelocity() - velocityMetersPerSecond()) < tolerance
    }
}

class WaitForSpinUpCommand(val velocityMetersPerSecond: () -> Double,
val tolerance: Double = Config.Launcher.Flywheel.velocityTolerance): CommandBase(){
    override fun execute() {
        println("Waiting for flywheel spin up...")
    }
    override fun isFinished(): Boolean {
        return abs(Flywheel.getVelocity() - velocityMetersPerSecond()) < tolerance
    }
}


class FlywheelCommand(val velocityMetersPerSecond: () -> Double): CommandBase(){
    init {
        addRequirements(Flywheel)
    }

    override fun initialize() {
        Flywheel.setFlywheel(velocityMetersPerSecond())
    }

    override fun execute() {
        SmartDashboard.putNumber("Flywheel Velocity", Flywheel.getVelocity())
    }

    override fun end(interrupted: Boolean) {
        Flywheel.flywheelOff()
    }
}

class FlywheelStopCommand():BlockCommand({Flywheel.flywheelOff()}, Flywheel)