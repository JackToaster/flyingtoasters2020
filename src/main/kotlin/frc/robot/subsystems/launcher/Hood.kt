package frc.robot.subsystems.launcher

import edu.wpi.first.wpilibj.LinearFilter
import edu.wpi.first.wpilibj.controller.PIDController
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.CommandBase
import edu.wpi.first.wpilibj2.command.SubsystemBase
import frc.robot.Config
import frc.robot.Config.Launcher.Hood.kD
import frc.robot.Config.Launcher.Hood.kI
import frc.robot.Config.Launcher.Hood.kP
import frc.robot.Config.Launcher.Hood.maxSetpoint
import frc.robot.Config.Launcher.Hood.minSetpoint
import frc.robot.Config.Launcher.Hood.positionTolerance

object Hood: SubsystemBase() {
    val servo = Config.Launcher.Hood.servo
    val pot = Config.Launcher.Hood.potentiometer

    val potFilter = LinearFilter.singlePoleIIR(0.1, 0.02)

    val pidController = PIDController(kP, kI, kD)

    init {
        setServo(0.0)
        //pidController.setIntegratorRange(-0.1, 0.1)
        pidController.setTolerance(positionTolerance)
        pidController.setpoint = minSetpoint
    }

    fun setServo(speed: Double){
        val servoValue = (-speed + 1.0) / 2.0 // Convert speed (-1 to 1) to servo value (0 to 1)

        servo.set(servoValue.coerceIn(0.0, 1.0))
        SmartDashboard.putNumber("Hood Servo Output", servoValue)
    }

    fun setSetpoint(setpoint: Double){
        pidController.setpoint = setpoint.coerceIn(minSetpoint, maxSetpoint)
    }

    fun getPosition(): Double {
        return potFilter.calculate(pot.get())
    }

    override fun periodic() {
        val measurement = getPosition()
        val output = pidController.calculate(measurement)
        setServo(output)

        SmartDashboard.putNumber("Hood Position", measurement)
        SmartDashboard.putNumber("Hood output", output)
        SmartDashboard.putBoolean("Hood at setpoint", pidController.atSetpoint())
    }
}

open class HoodTrackCommand(val setpoint: () -> Double): CommandBase() {
    init {
        addRequirements(Hood)
    }

    override fun initialize() {
        Hood.setSetpoint(setpoint())
    }

    override fun execute() {
        Hood.setSetpoint(setpoint())
    }

    override fun isFinished(): Boolean = Hood.pidController.atSetpoint()
}

class HoodCommand(setpoint: () -> Double): HoodTrackCommand(setpoint) {
    override fun isFinished(): Boolean = Hood.pidController.atSetpoint()
}