package frc.robot.subsystems.launcher

import edu.wpi.first.wpilibj.geometry.Rotation2d
import edu.wpi.first.wpilibj2.command.*
import frc.lib.commands.BlockCommand
import frc.lib.commands.IndefinitelyRepeatedCommand
import frc.lib.commands.Wait
import frc.robot.Config
import frc.robot.Config.Launcher.noVisionSetpoint
import frc.robot.autonomous.*
import frc.robot.subsystems.*
import frc.robot.subsystems.drive.Drive
import org.ghrobotics.lib.mathematics.twodim.geometry.Pose2d
import org.ghrobotics.lib.mathematics.twodim.geometry.Translation2d
import org.ghrobotics.lib.mathematics.twodim.geometry.toTransform
import org.ghrobotics.lib.mathematics.units.seconds

class AimAndSpinUpCommandNoVision(val setpoint: LauncherTarget) : ParallelCommandGroup(
        FlywheelSpinUpCommand({ setpoint.flywheelVelocity }),
        FieldRelativeTurretCommand({ setpoint.turretAngle }),
        HoodCommand({ setpoint.hoodPosition })
)

class AimAndSpinUpCommand() : ParallelCommandGroup(
        LedOnCommand(),
        SequentialCommandGroup(
                FlywheelSpinUpCommand({ Vision.getSetpoint().flywheelVelocity }),
                FlywheelCommand({ Vision.getSetpoint().flywheelVelocity })
        ),
        FieldRelativeHoldCommand({ Vision.getSetpoint().turretAngle }),
        PrepBallToShoot()
) {
    override fun execute() {
        super.execute()
    }
}

class ShootCommandNoVision(velocity: () -> Double, hood: () -> Double) : SequentialCommandGroup(
        ParallelDeadlineGroup(
                ParallelCommandGroup(// Deadline is spinning up, moving hood, and aiming
                        FlywheelSpinUpCommand(velocity),
                        HoodCommand(hood)
                ),
                PrepBallToShoot()
        )
        , ParallelCommandGroup(
        FeedBall(),
        FlywheelCommand(velocity)
)
) {
    override fun end(interrupted: Boolean) {
        super.end(interrupted)
        Flywheel.flywheelOff()
        Hood.setSetpoint(Config.Launcher.Hood.minSetpoint)
    }
}

class ShootCommand() : SequentialCommandGroup(
        LedOnCommand(),
        ParallelDeadlineGroup(
                ParallelCommandGroup(// Deadline is spinning up, moving hood, and aiming
                        FlywheelSpinUpCommand({ Vision.getSetpoint().flywheelVelocity }),
                        HoodCommand({ Vision.getSetpoint().hoodPosition }),
                        FieldRelativeTurretCommand({ Vision.getSetpoint().turretAngle }),
                        WaitForTargetLock()
                ),
                PrepBallToShoot()
        ),
        ParallelCommandGroup(
                SequentialCommandGroup(
                        Wait(0.5.seconds),
                        IndefinitelyRepeatedCommand(
                                SequentialCommandGroup(
                                        FeedOneBall(),
                                        //WaitForSpinUpCommand({ Vision.getSetpoint().flywheelVelocity })

                                        // TODO Try the following out:
                                        // should feed ball while waiting for spin up for a marginal time save.
                                        ParallelDeadlineGroup(
                                                WaitForSpinUpCommand({ Vision.getSetpoint().flywheelVelocity }),
                                                GetBall()
                                        )
                                )
                        )
                ),
                FlywheelCommand({ Vision.getSetpoint().flywheelVelocity }),
                FieldRelativeHoldCommand({ Vision.getSetpoint().turretAngle }),
                HoodTrackCommand({ Vision.getSetpoint().hoodPosition })
        )
) {
    override fun end(interrupted: Boolean) {
        super.end(interrupted)
        Flywheel.flywheelOff()
        Hood.setSetpoint(Config.Launcher.Hood.minSetpoint)
        // Turret.turretHome() // unnecessary and wastes time.
        VisionLEDs.set(false)
    }
}

class WaitForTargetLock() : CommandBase() {
    override fun isFinished(): Boolean {
        return Vision.hasLock()
    }
}

val trenchBackUpTrajectory = trajectory(
        waypoint(0.0, 0.0, 180.0),
        waypoint(0.5, 0.0, 180.0),
        config = trajectoryConfig(Speeds.Fast, backwards = true)
)

fun shootTargetZoneCommand() = ShootTargetZoneCommand()

class ShootTargetZoneCommand() : SequentialCommandGroup(
        ParallelDeadlineGroup(
                ParallelCommandGroup(
                        SequentialCommandGroup(
                                BlockCommand({Drive.setVelocities(-0.75, -0.75)}, Drive),
                                Wait(1.0.seconds),
                                BlockCommand({Drive.setVelocities(0.0, 0.0)}, Drive)
                        ),
//                        FollowTrajectory(
////                                trenchBackUpTrajectory.transformBy(Drive.getPose().toTransform())
//                        ),
                        ParallelCommandGroup(
                                FlywheelSpinUpCommand({ noVisionSetpoint.flywheelVelocity }),
                                HoodCommand({ noVisionSetpoint.hoodPosition }),
                                TurretCommand(Rotation2d(0.0))
                        )
                ),
                PrepBallToShoot()
        ),
        FeedBall()
){
    override fun end(interrupted: Boolean) {
        super.end(interrupted)
        BallFeeder.setSpeed(0.0)
        Flywheel.flywheelOff()

    }
}

// Target zone without backing up
class ShootStationaryCommand() : SequentialCommandGroup(
        ParallelDeadlineGroup(
                ParallelCommandGroup(
                        FlywheelSpinUpCommand({ noVisionSetpoint.flywheelVelocity }),
                        HoodCommand({ noVisionSetpoint.hoodPosition }),
                        TurretCommand(Rotation2d(0.0))
                ),
                PrepBallToShoot()
        ),
        FeedBall()
){
    override fun end(interrupted: Boolean) {
        super.end(interrupted)
        BallFeeder.setSpeed(0.0)
        Flywheel.flywheelOff()

    }
}


// Probably don't need ever, just use ShootCommand
//class ShootNoSpinUp(val velocity: Double): ParallelCommandGroup(
//    FeedBall(),
//    FlywheelCommand(velocity)
//)