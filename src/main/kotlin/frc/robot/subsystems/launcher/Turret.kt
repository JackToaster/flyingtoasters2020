package frc.robot.subsystems.launcher

import com.revrobotics.ControlType
import edu.wpi.first.wpilibj.geometry.Rotation2d
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.CommandBase
import edu.wpi.first.wpilibj2.command.SubsystemBase
import frc.robot.Config
import frc.robot.Config.Launcher.Turret.maxAccel
import frc.robot.Config.Launcher.Turret.maxAccelVision
import frc.robot.Config.Launcher.Turret.tolerance
import frc.robot.Config.Launcher.Turret.turretEncoder
import frc.robot.Config.Launcher.Turret.turretEncoderScaling
import frc.robot.Config.Launcher.Turret.turretMaxAngle
import frc.robot.Config.Launcher.Turret.turretMinAngle
import frc.robot.Config.Launcher.Turret.turretMotor
import frc.robot.Config.Launcher.Turret.turretPID
import frc.robot.subsystems.Vision
import frc.robot.subsystems.drive.Drive
import org.ghrobotics.lib.mathematics.units.derived.inDegrees
import kotlin.math.abs

object Turret: SubsystemBase() {
    val motor = turretMotor
    private val encoder = turretEncoder
    val pid = turretPID

    override fun periodic() {
        SmartDashboard.putNumber("turret angle", getPosition())
        SmartDashboard.putNumber("turret velocity", getVelocity())

        if(Vision.hasLock()){
            pid.setSmartMotionMaxAccel(maxAccelVision, 0)
        }else{

            pid.setSmartMotionMaxAccel(maxAccel, 0)
        }
    }

    fun getPosition() = encoder.position / turretEncoderScaling
    fun getVelocity() = encoder.velocity / turretEncoderScaling * 60.0

    fun turretOff(){
        motor.set(0.0)
    }

    fun turretHome() {
        pid.setReference(0.0, ControlType.kSmartMotion)
    }

    fun getFieldRelativeAngle() =
            Rotation2d.fromDegrees(getPosition())
            .plus(Drive.getPose()?.rotation ?: Rotation2d())

    fun getAngle() = Rotation2d.fromDegrees(getPosition())

    fun fieldRelativeToRobotRelativeAngle(angle: Rotation2d, robotAngle: Rotation2d): Rotation2d =
            angle.minus(robotAngle)

    fun setFieldRelativeAngle(angle: Rotation2d) {
        // Convert field angle to robot-relative turret angle
        val robotRelativeAngle = fieldRelativeToRobotRelativeAngle(angle, Drive.getPose()?.rotation ?: Rotation2d())
        SmartDashboard.putNumber("Turret set angle", angle.degrees)

        setRobotRelativeAngle(robotRelativeAngle)
    }

    fun setRobotRelativeAngle(angle: Rotation2d) {
        var degrees = angle.degrees // maybe -?

        // Try to get the angle within the turret range
        while(degrees > turretMaxAngle) degrees -= 360
        while(degrees < turretMinAngle) degrees += 360

        if(degrees >= turretMinAngle && degrees <= turretMaxAngle){
            pid.setReference(degrees * turretEncoderScaling, ControlType.kSmartMotion)
        } else {
            pid.setReference(closestEndpointTo(angle) * turretEncoderScaling, ControlType.kSmartMotion)
        }
    }

    fun closestEndpointTo(angle: Rotation2d): Double{
        val minEndpointDistance = abs(angle.minus(Rotation2d.fromDegrees(turretMinAngle)).degrees)
        val maxEndpointDistance = abs(angle.minus(Rotation2d.fromDegrees(turretMaxAngle)).degrees)
        return if(minEndpointDistance < maxEndpointDistance) {
            turretMinAngle
        } else {
            turretMaxAngle
        }
    }
}

class TurretDoNothingCommand: CommandBase() {
    init {
        addRequirements(Turret)
    }

    override fun initialize() {
        Turret.turretOff()
    }
}

class TurretCommand(val target: Rotation2d): CommandBase() {
    init {
        addRequirements(Turret)
    }

    override fun isFinished(): Boolean {
        val diff = Turret.getAngle().minus(target).degrees
        //println(diff)
        return abs(diff) < tolerance.inDegrees()
    }

    override fun initialize() {
        Turret.setRobotRelativeAngle(target)
    }
}

class TurretHoldCommand(val target: Rotation2d): CommandBase() {
    init {
        addRequirements(Turret)
    }

    override fun initialize() {
        Turret.setRobotRelativeAngle(target)
    }
}

open class FieldRelativeHoldCommand(val target: () -> Rotation2d): CommandBase() {
    init {
        addRequirements(Turret)
    }

    override fun initialize() {
        Turret.setFieldRelativeAngle(target())
    }

    override fun execute() {
        Turret.setFieldRelativeAngle(target())
    }
}

class FieldRelativeTurretCommand(target: () -> Rotation2d): FieldRelativeHoldCommand(target) {
    override fun isFinished(): Boolean {
        val diff = Turret.getFieldRelativeAngle().minus(target()).degrees
        return abs(diff) < tolerance.inDegrees()
    }
}
