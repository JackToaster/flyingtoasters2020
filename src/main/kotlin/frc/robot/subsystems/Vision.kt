package frc.robot.subsystems

import edu.wpi.first.networktables.*
import edu.wpi.first.wpilibj.geometry.Pose2d
import edu.wpi.first.wpilibj.geometry.Rotation2d
import edu.wpi.first.wpilibj.geometry.Transform2d
import edu.wpi.first.wpilibj.geometry.Translation2d
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.SubsystemBase
import frc.lib.mathematics.Interpolatable
import frc.lib.mathematics.MedianFilter
import frc.robot.Config.Launcher.Turret.horizontalScaling
import frc.robot.Config.Launcher.Turret.visionLatency
import frc.robot.Config.Launcher.Turret.visionScaling
import frc.robot.Config.Launcher.maxInnerPortAngle
import frc.robot.Config.Launcher.maxInnerPortDistance
import frc.robot.Config.Launcher.noVisionSetpoint
import frc.robot.Config.Launcher.outerToInnerPortDistanceM
import frc.robot.Config.Launcher.setpoints
import frc.robot.subsystems.drive.Drive
import frc.robot.subsystems.launcher.Turret
import org.ghrobotics.lib.mathematics.lerp
import org.ghrobotics.lib.mathematics.units.Frac
import org.ghrobotics.lib.mathematics.units.SIUnit
import org.ghrobotics.lib.mathematics.units.Second
import org.ghrobotics.lib.mathematics.units.derived.*
import org.ghrobotics.lib.mathematics.units.inMeters
import org.ghrobotics.lib.mathematics.units.operations.times
import kotlin.math.abs

// Used in converting distance to launcher settings
class LauncherSetpoint(val flywheelVelocity: Double,
                       val hoodPosition: Double,
                       val turretOffsetDeg: Double)
    : Interpolatable<LauncherSetpoint> {
    override fun interpolate(other: LauncherSetpoint, alpha: Double): LauncherSetpoint {
       return LauncherSetpoint(
               flywheelVelocity.lerp(other.flywheelVelocity, alpha),
               hoodPosition.lerp(other.hoodPosition, alpha),
               turretOffsetDeg.lerp(other.turretOffsetDeg, alpha)
               )
    }
}

// Where to actually move everything
data class LauncherTarget(val flywheelVelocity: Double,
                          val hoodPosition: Double,
                          val turretAngle: Rotation2d)

// Targeting data from vision
data class TargetingData(val distance: Double, val fieldRelativeAngle: Rotation2d)

fun getSetpointFromTargetingData(targetingData: TargetingData): LauncherTarget? {
    val setpoint = setpoints.get(targetingData.distance) ?: return null
    return LauncherTarget(
            flywheelVelocity = setpoint.flywheelVelocity,
            hoodPosition = setpoint.hoodPosition,
            turretAngle = Rotation2d.fromDegrees(setpoint.turretOffsetDeg).plus(targetingData.fieldRelativeAngle)
    )
}

fun correctVisionPoseForLatency(angularVelocity: SIUnit<Frac<Radian, Second>>, latency: SIUnit<Second>, offset: Pose2d): Pose2d {
    val latencyCorrection = Rotation2d(-angularVelocity.times(latency).inRadians())
    return Pose2d(offset.translation.rotateBy(latencyCorrection), offset.rotation.plus(latencyCorrection))
}

fun visionCoordsToFieldCoords(visionCoords: Pose2d, driveBasePose: Pose2d, turretAngle: Rotation2d): Pose2d {
    return Pose2d(driveBasePose.translation, driveBasePose.rotation.plus(turretAngle)).transformBy(
            Transform2d(visionCoords.translation, Rotation2d()) //visionCoords.rotation)
    )
}

// Takes the position of the target and drivebase, then decides whether to shoot at the inner or outer port
// and returns the distance/angle (TargetingData) for that target
fun getSelectedTargetingData(pose: Pose2d, drivePose: Pose2d) : TargetingData {
    val targetOffset = pose.translation.minus(drivePose.translation)
    val targetOffsetAngle = Rotation2d(targetOffset.x, targetOffset.y)
    val targetAngle = pose.rotation.minus(targetOffsetAngle)
    val targetDistance = targetOffset.norm

    return if(targetDistance < maxInnerPortDistance.inMeters() &&
            abs(targetAngle.degrees) < maxInnerPortAngle.inDegrees()){
        SmartDashboard.putBoolean("Targeting Inner Port", true)
        // Get the offset of the inner port
        val innerTargetOffset = targetOffset.plus(Translation2d(-outerToInnerPortDistanceM, 0.0))
        val innerTargetOffsetAngle = Rotation2d(targetOffset.x, targetOffset.y)
        val innerTargetDistance = innerTargetOffset.norm
        TargetingData(innerTargetDistance, innerTargetOffsetAngle)
    } else {
        SmartDashboard.putBoolean("Targeting Inner Port", false)
        // get the offset of the outer port
        TargetingData(targetDistance, targetOffsetAngle)
    }
}

object Vision : SubsystemBase() {
    private val ntInstance = NetworkTableInstance.getDefault()
    private val table = ntInstance.getTable("OpenSight")

    val positionXEntry = table.getEntry("x")
    val positionYEntry = table.getEntry("y")
    val angleEntry = table.getEntry("TargetAngle")
    val successEntry = table.getEntry("Success")

    var targetPose: Pose2d = Pose2d()

    val turretAngleFilter = MedianFilter(10)

    val targetingData: TargetingData?
        get() = getSelectedTargetPosition()

    init {
        positionXEntry.addListener(
                {
                    updateFromNT()
                }, EntryListenerFlags.kNew or EntryListenerFlags.kUpdate)
    }

    fun hasLock() = successEntry.getBoolean(false)

    fun getSetpoint() = targetingData?.let{getSetpointFromTargetingData(it)} ?: noVisionSetpoint

    override fun periodic() {
        SmartDashboard.putNumber("Target X", targetPose?.translation?.x ?: 0.0)
        SmartDashboard.putNumber("Target Y", targetPose?.translation?.y ?: 0.0)
        SmartDashboard.putNumber("Target Angle", targetPose?.rotation?.degrees ?: 0.0)

        val data = targetingData
        SmartDashboard.putNumber("Target dist", data?.distance ?: 0.0)
        SmartDashboard.putNumber("Target angle", data?.fieldRelativeAngle?.degrees ?: 0.0)
    }

    // Every time a Network Tables value is updated, this function is called.
    private fun updateFromNT() {
        if(successEntry.getBoolean(false)) {
            val offset = Pose2d(
                    Translation2d(
                            positionXEntry.getDouble(0.0) * visionScaling,
                            positionYEntry.getDouble(0.0) * visionScaling * horizontalScaling
                    ),
                    Rotation2d.fromDegrees(angleEntry.getDouble(0.0))
            )

            val angularVelocity = Turret.getVelocity().degrees.velocity + Drive.getAngularVelocity().degrees.velocity // Degrees per second

            val latencyCorrectedOffset = correctVisionPoseForLatency(angularVelocity, visionLatency, offset)

            SmartDashboard.putNumber("Offset x", latencyCorrectedOffset.translation.x)
            SmartDashboard.putNumber("Offset y", latencyCorrectedOffset.translation.y)
            SmartDashboard.putNumber("Offset angle", latencyCorrectedOffset.rotation.degrees)

            // Transform the target position into field coordinates
            targetPose = visionCoordsToFieldCoords(latencyCorrectedOffset, Drive.getPose() ?: Pose2d(), Turret.getAngle())
        } else {
            //turretAngleFilter.pop()
        }
    }

    private fun getSelectedTargetPosition() : TargetingData? {
        val pose = targetPose ?: return null
        val drivePose = Drive.getPose() ?: return null

        val data = getSelectedTargetingData(pose, drivePose)
        turretAngleFilter.push(data.fieldRelativeAngle.degrees)
        return TargetingData(data.distance, Rotation2d.fromDegrees(turretAngleFilter.calculate() ?: noVisionSetpoint.turretAngle.degrees))
    }
}