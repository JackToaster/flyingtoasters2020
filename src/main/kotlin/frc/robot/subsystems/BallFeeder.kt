package frc.robot.subsystems

import com.ctre.phoenix.motorcontrol.TalonFXControlMode
import edu.wpi.first.wpilibj.Timer
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.*
import frc.lib.commands.Wait
import frc.robot.Config
import frc.robot.Config.BallFeeder.getBallSpeed
import frc.robot.Config.BallFeeder.pulseDuration
import frc.robot.Config.BallFeeder.pulseSpeed
import frc.robot.Config.BallFeeder.pulseTime
import frc.robot.Config.BallFeeder.retractSpeed
import org.ghrobotics.lib.mathematics.units.inSeconds
import org.ghrobotics.lib.mathematics.units.milliseconds

object BallFeeder : SubsystemBase() {
    val feederMotor = Config.BallFeeder.feederMotor
    val ballSensor = Config.BallFeeder.sensor

    fun setSpeed(speed: Double){
        feederMotor.set(TalonFXControlMode.PercentOutput, speed)
    }

    fun isThereABall(): Boolean {
        return ballSensor.get()
    }

    override fun periodic() {
        SmartDashboard.putBoolean("Is there a ball", isThereABall())
    }
}

class FeederCommand(val speed: Double): CommandBase() {
    init {
        addRequirements(BallFeeder)
    }

    override fun initialize() {
        BallFeeder.setSpeed(speed)
    }

    override fun end(interrupted: Boolean) {
        BallFeeder.setSpeed(0.0)
    }
}

class GetBall: CommandBase() {
    var startTime = 0.0
    init {
        addRequirements(BallFeeder)
    }

    override fun initialize() {
        BallFeeder.setSpeed(Config.BallFeeder.getBallSpeed)
        startTime = Timer.getFPGATimestamp()
    }

    override fun execute() {
        val time = Timer.getFPGATimestamp()
        if((time - startTime) % pulseTime > pulseTime - pulseDuration) BallFeeder.setSpeed(pulseSpeed)
        else BallFeeder.setSpeed(Config.BallFeeder.getBallSpeed)
    }

    override fun isFinished(): Boolean {
        return BallFeeder.isThereABall()
    }

    override fun end(interrupted: Boolean) {
        BallFeeder.setSpeed(0.0)
    }
}

class FeedBall: CommandBase() {
    var startTime = 0.0
    init {
        addRequirements(BallFeeder)
    }

    override fun initialize() {
        BallFeeder.setSpeed(Config.BallFeeder.feedBallSpeed)
        startTime = Timer.getFPGATimestamp()
    }

    override fun execute() {
        val time = Timer.getFPGATimestamp()
        if((time - startTime) % pulseTime > pulseTime - pulseDuration) BallFeeder.setSpeed(pulseSpeed)
        else BallFeeder.setSpeed(Config.BallFeeder.feedBallSpeed)
    }

    override fun end(interrupted: Boolean) {
        BallFeeder.setSpeed(0.0)
    }
}

class FeedOneBall: SequentialCommandGroup(GetBallNoWait(), FeedBallSingle())

private class GetBallNoWait: CommandBase() {
    var startTime = 0.0
    init {
        addRequirements(BallFeeder)
    }

    override fun initialize() {
        BallFeeder.setSpeed(Config.BallFeeder.feedBallSpeed)
        startTime = Timer.getFPGATimestamp()
    }

    override fun execute() {
        val time = Timer.getFPGATimestamp()
        if((time - startTime) % pulseTime > pulseTime - pulseDuration) BallFeeder.setSpeed(pulseSpeed)
        else BallFeeder.setSpeed(Config.BallFeeder.feedBallSpeed)
    }

    override fun isFinished(): Boolean {
        return BallFeeder.isThereABall()
    }

    override fun end(interrupted: Boolean) {
        if(interrupted){
            BallFeeder.setSpeed(0.0)
        }
    }
}

private class FeedBallSingle: CommandBase() {
    var startTime: Double = 0.0
    init {
        addRequirements(BallFeeder)
    }

    override fun initialize() {
        BallFeeder.setSpeed(Config.BallFeeder.feedBallSpeed)
    }

    override fun execute() {
        BallFeeder.setSpeed(Config.BallFeeder.feedBallSpeed)
    }

    override fun isFinished(): Boolean {
        return !BallFeeder.isThereABall()
    }

    override fun end(interrupted: Boolean) {
        BallFeeder.setSpeed(0.0)
    }
}

class PrepBallToShoot: SequentialCommandGroup(
        GetBall(),
        ParallelRaceGroup(
                FeederCommand(retractSpeed),
                Wait(50.milliseconds)
        )
)