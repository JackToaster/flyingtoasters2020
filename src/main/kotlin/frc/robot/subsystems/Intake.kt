package frc.robot.subsystems

import com.ctre.phoenix.motorcontrol.TalonFXControlMode
import com.revrobotics.ControlType
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.*
import frc.lib.commands.BlockCommand
import frc.lib.commands.Wait
import frc.robot.Config
import frc.robot.Config.Intake.extendedPosition
import frc.robot.Config.Intake.extenderPID
import frc.robot.Config.Intake.intakeEjectSpeed
import frc.robot.Config.Intake.intakeRetractTime
import frc.robot.Config.Intake.intakeSpeed
import frc.robot.Config.Intake.retractedPosition
import org.ghrobotics.lib.mathematics.units.inSeconds
import org.ghrobotics.lib.mathematics.units.milliseconds

object Intake : SubsystemBase(){
    val intakeMotor = Config.Intake.intakeMotor
    val extendMotor = Config.Intake.extenderMotor
    val extendEncoder = extendMotor.encoder

    override fun periodic() {
        SmartDashboard.putNumber("Extender pos.", extendEncoder.position)
    }

    fun setSpeed(speed: Double) {
        Intake.intakeMotor.set(speed)
    }

    fun setExtendingDistance(extended:Boolean){
        if(extended){
            extenderPID.setReference(extendedPosition, ControlType.kSmartMotion)
        }else{
            extenderPID.setReference(retractedPosition, ControlType.kSmartMotion)
        }
    }

//name:type
}

class IntakeCommand: CommandBase() {
    init {
        addRequirements(Intake)
    }

    override fun initialize() {
        Intake.setSpeed(intakeSpeed)
        Intake.setExtendingDistance(true)
    }
}

class IntakeEjectCommand: CommandBase() {
    init {
        addRequirements(Intake)
    }

    override fun initialize() {
        Intake.setSpeed(intakeEjectSpeed)
        Intake.setExtendingDistance(true)
    }

    override fun end(interrupted: Boolean) {
        Intake.setSpeed(0.0)
        Intake.setExtendingDistance(false)
    }
}

class IntakeRetractCommand: SequentialCommandGroup(
        InstantCommand(Runnable {
            Intake.setSpeed(intakeSpeed)
            Intake.setExtendingDistance(false)}, Intake),
        WaitCommand(intakeRetractTime.inSeconds()),
        InstantCommand(Runnable {Intake.setSpeed(0.0)}, Intake)
)

// Resets the intake by running it to an end stop and then zeroing the encoder.
class IntakeResetCommand: SequentialCommandGroup(
        BlockCommand({Intake.extendMotor.set(-0.2)}, Intake),
        Wait(500.milliseconds),
        BlockCommand({
            Intake.extendMotor.set(0.0)
            Intake.extendEncoder.position = 0.0
        }, Intake)
)