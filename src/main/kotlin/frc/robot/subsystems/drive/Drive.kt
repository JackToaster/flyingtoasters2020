package frc.robot.subsystems.drive

import com.ctre.phoenix.motorcontrol.ControlMode
import com.ctre.phoenix.motorcontrol.DemandType
import com.ctre.phoenix.motorcontrol.can.TalonFX
import edu.wpi.first.wpilibj.Timer
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward
import edu.wpi.first.wpilibj.geometry.Pose2d
import edu.wpi.first.wpilibj.geometry.Rotation2d
import edu.wpi.first.wpilibj.geometry.Translation2d
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveOdometry
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.SubsystemBase
import frc.robot.Config
import frc.robot.Config.Drive.encoderRateToVelocity
import frc.robot.Config.Drive.metersPerEncoderEdge
import frc.robot.Config.Drive.pigeon
import frc.robot.Config.Drive.pigeonUseFusedHeading
import frc.robot.controls.ControlMapping
import org.ghrobotics.lib.mathematics.lerp
import org.ghrobotics.lib.mathematics.signedPow
import org.ghrobotics.lib.mathematics.units.inMeters
import kotlin.math.IEEErem
import kotlin.math.min


object Drive: SubsystemBase(){
    private val left = Config.Drive.left1
    private val right = Config.Drive.right1
    val pigeon = Config.Drive.pigeon

    private val odometry = DifferentialDriveOdometry(Rotation2d(getHeading()))

    val feedforward = SimpleMotorFeedforward(
            Config.Drive.Characterization.kS,
            Config.Drive.Characterization.kV,
            Config.Drive.Characterization.kA
    )

    var lastFeedforwardTime = 0.0
    var lastVelocities = DifferentialDriveWheelSpeeds(0.0, 0.0)

    // initializer block
    init {
        defaultCommand = TeleopDriveCommand()
        resetOdometry()
    }

    fun resetOdometry(position: Translation2d = Translation2d()) {
        pigeon.fusedHeading = 0.0
        odometry.resetPosition(Pose2d(position, Rotation2d(0.0)), Rotation2d(0.0))
        left.selectedSensorPosition = 0
        right.selectedSensorPosition = 0
    }

    private fun getEncoder(motor: TalonFX) = metersPerEncoderEdge * motor.selectedSensorPosition
    fun getLeftEncoder() = getEncoder(left)
    fun getRightEncoder() = -getEncoder(right)

    // val differentialDrive = DifferentialDrive(left, right)
    fun getHeading(): Double {
        if(pigeonUseFusedHeading) {
            return pigeon.fusedHeading.IEEErem(360.0) * -1.0
        } else {
            var xyz: DoubleArray = doubleArrayOf(0.0, 0.0, 0.0)
            pigeon.getAccumGyro(xyz)
            return xyz[2].IEEErem(360.0) * -1.0
        }
    }

    fun getAngularVelocity(): Double {
        var xyz: DoubleArray = doubleArrayOf(0.0, 0.0, 0.0)
        pigeon.getRawGyro(xyz)
        return xyz[2] * -1.0
    }

    fun getWheelSpeeds(): DifferentialDriveWheelSpeeds? {
        return DifferentialDriveWheelSpeeds(
                (encoderRateToVelocity * left.selectedSensorVelocity).value,
                -(encoderRateToVelocity * right.selectedSensorVelocity).value
        )
    }

    fun getPose(): Pose2d = odometry.poseMeters

    override fun periodic() { // Update the odometry in the periodic block
        // TODO Re-verify this after fixing getEncoder
        odometry.update(Rotation2d.fromDegrees(getHeading()), -getLeftEncoder().inMeters(),
                getRightEncoder().inMeters())

        SmartDashboard.putNumber("Odometry X", getPose()?.translation?.x ?: 0.0)
        SmartDashboard.putNumber("Odometry Y", getPose()?.translation?.y ?: 0.0)
        SmartDashboard.putNumber("Odometry Angle", getPose()?.rotation?.degrees ?: 0.0)
        SmartDashboard.putNumber("Angular vel of drivetrain", getAngularVelocity())

        //println("X: ${getPose()?.translation?.x}, Y: ${getPose()?.translation?.y}, degrees: ${getPose()?.rotation?.degrees}")
    }

    fun setVelocities(leftVel: Double, rightVel: Double){
        val time = Timer.getFPGATimestamp()
        val deltaTime = time - lastFeedforwardTime
        lastFeedforwardTime = time

        val leftDeltaVelocity = leftVel - lastVelocities.leftMetersPerSecond
        val rightDeltaVelocity = rightVel - lastVelocities.rightMetersPerSecond
        lastVelocities = DifferentialDriveWheelSpeeds(leftVel, rightVel)

        val leftAcceleration = leftDeltaVelocity / deltaTime
        val rightAcceleration = rightDeltaVelocity / deltaTime

        val leftFeedforward = feedforward.calculate(leftVel, leftAcceleration)
        val rightFeedforward = feedforward.calculate(rightVel, rightAcceleration)

        // Feedforward only
//        left.set(ControlMode.PercentOutput, -leftFeedforward / 12.0)
//        right.set(ControlMode.PercentOutput, -rightFeedforward / 12.0)


        left.set(ControlMode.Velocity, -leftVel / encoderRateToVelocity.value,
                DemandType.ArbitraryFeedForward, -leftFeedforward / 12.0)

        right.set(ControlMode.Velocity, -rightVel / encoderRateToVelocity.value,
                DemandType.ArbitraryFeedForward, -rightFeedforward / 12.0)
    }

    fun driveTank(leftInput : Double, rightInput : Double){
        left.set(ControlMode.PercentOutput, leftInput)
        right.set(ControlMode.PercentOutput, rightInput)
    }

    fun driveTankVolts(leftVolts: Double, rightVolts: Double) {
        driveTank(leftVolts / 12.0, rightVolts / 12.0)
    }

    fun driveArcade(power : Double, turn : Double) =
            driveTank((power + turn).coerceIn(-1.0, 1.0), (power - turn).coerceIn(-1.0, 1.0))

    fun driveGrilledCheese(power : Double, rotation : Double){
        with(ControlMapping){
            val expPower = power.signedPow(kSpeedExponent)
            val expRotation = rotation.signedPow(kTurnExponent)

            val arcadeRotation = expRotation * kArcadeRotationGain
            val curvatureRotation = expRotation * expPower * kCurvatureGain

            val ratio = min(expPower / kArcadeThreshold, 1.0)

            val outputRotation = arcadeRotation.lerp(curvatureRotation, ratio)
            val outputPower = expPower * kSpeedGain

            driveArcade(outputPower, outputRotation)
        }
    }
}