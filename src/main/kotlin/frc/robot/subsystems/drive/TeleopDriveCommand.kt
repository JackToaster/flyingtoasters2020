package frc.robot.subsystems.drive

import edu.wpi.first.wpilibj2.command.Command
import edu.wpi.first.wpilibj2.command.CommandBase
import edu.wpi.first.wpilibj2.command.Subsystem
import frc.robot.controls.ControlMapping
import frc.robot.controls.PS4


class TeleopDriveCommand : CommandBase() {
    init {
        addRequirements(Drive)
    }

    override fun execute() {
        Drive.driveGrilledCheese(ControlMapping.drivePower(), ControlMapping.driveTurn())
    }
}