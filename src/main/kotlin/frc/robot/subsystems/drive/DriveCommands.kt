package frc.robot.subsystems.drive

import com.ctre.phoenix.music.Orchestra
import edu.wpi.first.wpilibj2.command.InstantCommand
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup
import frc.lib.commands.BlockCommand
import frc.lib.commands.Wait
import frc.robot.Config
import org.ghrobotics.lib.mathematics.units.seconds

class StopDrive: InstantCommand(Runnable{ Drive.setVelocities(0.0, 0.0) }, Drive)
