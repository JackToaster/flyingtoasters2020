package frc.robot.subsystems

import com.ctre.phoenix.motorcontrol.ControlMode
import com.ctre.phoenix.motorcontrol.NeutralMode
import edu.wpi.first.wpilibj.geometry.Rotation2d
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj2.command.CommandBase
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup
import edu.wpi.first.wpilibj2.command.SubsystemBase
import frc.lib.commands.BlockCommand
import frc.robot.Config.Climber.climberMotor
import frc.robot.Config.Climber.extendedPosition
import frc.robot.Config.Climber.positionTolerance
import frc.robot.Config.Climber.startHoldPosition
import frc.robot.controls.ControlMapping
import frc.robot.subsystems.drive.Drive
import frc.robot.subsystems.launcher.Turret
import frc.robot.subsystems.launcher.TurretCommand
import frc.robot.subsystems.launcher.TurretDoNothingCommand
import kotlin.math.abs

object Climber : SubsystemBase() {
    private val motor = climberMotor
    var setpoint = 0.0

    init {
        motor.set(ControlMode.Current, -5.0)
    }

    fun canExtend() = motor.selectedSensorPosition < extendedPosition

    fun setCoastMode(){
        motor.setNeutralMode(NeutralMode.Coast)
    }

    fun setClimber(power: Double) {
        motor.set(ControlMode.PercentOutput, power)
        SmartDashboard.putNumber("Climber Power", power)
    }

    fun setPosition(position: Double){
        motor.set(ControlMode.Position, position)
        setpoint = position

        SmartDashboard.putNumber("Climber Target Position", position)
    }

    fun atSetpoint(): Boolean {
        return abs(motor.selectedSensorPosition - setpoint) < positionTolerance
    }

    override fun periodic() {
        SmartDashboard.putNumber("Climber Position", motor.getSelectedSensorPosition(0).toDouble())

        SmartDashboard.putBoolean("Climber at setpoint", atSetpoint())

        SmartDashboard.putBoolean("Climber can extend", canExtend())
    }
}

class TeleopClimbCommand: SequentialCommandGroup(
        BlockCommand({
            Turret.defaultCommand = TurretDoNothingCommand()
        }, Turret),
        TurretCommand(Rotation2d.fromDegrees(-90.0)),
        ClosedLoopClimberCommand(extendedPosition),
        ClimberCommand()
)


private class ClimberCommand: CommandBase() {
    init {
        addRequirements(Climber)
    }

    override fun execute() {
        Climber.setClimber(ControlMapping.climberPower())
    }

    override fun end(interrupted: Boolean) {
        Climber.setClimber(0.0)
    }
}

private class ClosedLoopClimberCommand(val position: Double): CommandBase() {
    init {
        addRequirements(Climber)
    }

    override fun initialize() {
        if(!Climber.canExtend()){
            cancel()
        } else {
            Climber.setPosition(position)
        }
    }

    override fun execute() {
        if(!Climber.canExtend()){
            cancel()
        } else {
            Climber.setPosition(position)
        }
    }

    override fun isFinished(): Boolean {
        return Climber.atSetpoint() || !Climber.canExtend()
    }

    override fun end(interrupted: Boolean) {
        Climber.setClimber(0.0)
    }
}

class TeleopClimberRetractCommand: CommandBase() {
    init {
        addRequirements(Climber)
    }

    override fun initialize() {
        Climber.setCoastMode()
    }

    override fun execute() {
        Climber.setClimber(-ControlMapping.climberPower())
    }

    override fun end(interrupted: Boolean) {
        Climber.setClimber(0.0)
    }
}