/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot

import com.ctre.phoenix.motorcontrol.NeutralMode
import com.ctre.phoenix.motorcontrol.VelocityMeasPeriod
import com.ctre.phoenix.motorcontrol.can.TalonFX
import com.ctre.phoenix.sensors.PigeonIMU
import edu.wpi.first.wpilibj.DigitalInput
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveKinematics
import frc.lib.config.*
import frc.robot.autonomous.Speeds
import org.ghrobotics.lib.mathematics.units.derived.velocity
import com.revrobotics.CANSparkMax
import com.revrobotics.CANSparkMaxLowLevel
import edu.wpi.first.wpilibj.AnalogPotentiometer
import edu.wpi.first.wpilibj.Servo
import edu.wpi.first.wpilibj.geometry.Rotation2d
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import frc.lib.mathematics.interpolatingLookupFrom
import frc.robot.subsystems.LauncherSetpoint
import frc.robot.subsystems.LauncherTarget
import org.ghrobotics.lib.mathematics.units.*
import org.ghrobotics.lib.mathematics.units.derived.degrees
import kotlin.math.PI


/**
 * The Config class provides a convenient place for teams to hold robot-wide
 * constants.
 *
 * It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */

object Config {
    object Trajectories {
        const val safetyMode = false
        val defaultSpeed = Speeds.Med
    }

    object Drive {
        val left1 = TalonFX(12)
        val left2 = TalonFX(13)

        val right1 = TalonFX(2)
        val right2 = TalonFX(3)

        private const val falconEPR = 2048 // Edges per revolution for falcon 500
        private const val ratio = 64.0 / 8.0 // Gear ratio of drivetrain
        private val wheelCircumference = 5.0.inches * PI

        // Distance (in meters) driven after one encoder pulse
        val metersPerEncoderEdge = wheelCircumference / (falconEPR * ratio)

        // velocity (in meters/s) per velocity unit
        val encoderRateToVelocity = metersPerEncoderEdge.velocity * 10.0

        // Characterization values, do not change
        object Characterization {
            const val kS = 0.465 // Volts
            const val kV = 2.18 // Volts per m/s (Volt seconds per meter)
            const val kA = 0.118 // Volts per m/s^2 (Volt seconds^2 per meter)
            const val trackWidth = 0.864625420561114 // Meters

            // TODO Re-enable feedback
            const val kP = 0.170 // From characterization: 0.186 // for position feedback loop: 0.0443
            const val kD = 0.0 // for position feedback loop: 18.8

            val driveKinematics = DifferentialDriveKinematics(trackWidth)
        }


        // Maximum speed and acceleration during path following
        const val maxSpeedMetersPerSecond = 2.0
        const val maxAccelerationMetersPerSecondSquared = 2.0

        // Reasonable baseline values for a RAMSETE follower in units of meters and seconds
        const val ramseteB = 2.0
        const val ramseteZeta = 0.7

        init {
            left2 follows left1
            right2 follows right1

            configure(left1, left2, right1, right2){
                limitSupplyCurrent(55.amps)
                configOpenloopRamp(0.1)
                setNeutralMode(NeutralMode.Brake)

                enableVoltageCompensation(true)
                configVoltageCompSaturation(12.0)

                config_kP(0, Characterization.kP)
                config_kD(0, Characterization.kD)
            }

            configure(right1, right2){
                inverted = true;
            }
        }

        fun enableCoastMode(){
            configure(left1, left2, right1, right2){
                setNeutralMode(NeutralMode.Coast)
            }
        }

        fun disableCoastMode(){
            configure(left1, left2, right1, right2){
                setNeutralMode(NeutralMode.Brake)
            }
        }

        val pigeon = PigeonIMU(0)
        const val pigeonUseFusedHeading = true
    }

    object BallFeeder {
        val feederMotor = TalonFX(30)
        val sensor = DigitalInput(1)

        const val getBallSpeed = 0.25
        const val retractSpeed = -0.3
        const val feedBallSpeed = 1.0

        const val pulseSpeed = -0.2

        const val pulseTime = 3.0
        const val pulseDuration = 0.25

        init {
            feederMotor.inverted = true
            feederMotor.limitSupplyCurrent(40.amps)
        }
    }

    object Intake {
        val intakeMotor = CANSparkMax(4, CANSparkMaxLowLevel.MotorType.kBrushless)
        val extenderMotor = CANSparkMax(11, CANSparkMaxLowLevel.MotorType.kBrushless)
        val extenderPID = extenderMotor.pidController

        const val intakeSpeed = 1.0
        val intakeRetractTime = 0.5.seconds

        const val intakeEjectSpeed = -0.5

        // approximately centimeters
        const val retractedPosition = 7.0
        const val extendedPosition = 25.5 // 24.5

        init {
            extenderPID.p = 0.00008
            extenderPID.d = 0.000
            extenderPID.setSmartMotionMaxVelocity(10000.0, 0)
            extenderPID.setSmartMotionMaxAccel(50000.0, 0)
            extenderMotor.encoder.position = 0.0

            with(extenderMotor){
                setSmartCurrentLimit(10,10)
            }

            with(intakeMotor){
                setSmartCurrentLimit(30)
            }
        }
    }

    object Launcher {
        val maxInnerPortDistance = 6.0.meters // Max distance to aim for the inner port
        val maxInnerPortAngle = 20.degrees // Max angle to aim for the inner port

        const val outerToInnerPortDistanceM = 0.74295 //Distance (meters) from outer to inner port

        val setpoints = interpolatingLookupFrom(
                3.0 to LauncherSetpoint(20.5, 16.0, -1.5),
                4.0 to LauncherSetpoint(23.7, 18.0, -1.0)
//                5.0 to LauncherSetpoint(24.9, 19.5, 0.5),
//                6.0 to LauncherSetpoint(27.6, 21.2, 1.3),
//                //6.5 to LauncherSetpoint(28.0, 20.3, -1.5),
//                6.9 to LauncherSetpoint(29.5, 21.0, 1.5),
//                9.0 to LauncherSetpoint(33.9, 22.5, 2.0)
        )

        // Protected zone or spin up without vision
        val noVisionSetpoint = LauncherTarget(20.0, Hood.minSetpoint, Rotation2d.fromDegrees(180.0))

        object Flywheel {
            val flywheelMaster = TalonFX(14) // TODO IDs
            val flywheelFollower = TalonFX(15)

            val velocityTolerance = 0.25

            private const val falconEPR = 2048 // Edges per revolution for falcon 500
            private const val ratio = 28.0 / 40.0 // Gear ratio of drivetrain
            private val wheelCircumference = 4.0.inches * PI

            // Distance (in meters) driven after one encoder pulse
            private val metersPerEncoderEdge = wheelCircumference / (falconEPR * ratio)

            // velocity (in meters/s) per velocity unit
            val encoderRateToVelocity = metersPerEncoderEdge.velocity * 10.0

            val kV = 0.25

            val kP = 1.0

            private val shootDefaultVelocity = 25.0
            val shootVelocity  = { SmartDashboard.getNumber("Shooter Velocity", shootDefaultVelocity) }

            init {
                SmartDashboard.putNumber("Shooter Velocity", shootDefaultVelocity)

                flywheelFollower follows flywheelMaster
                flywheelFollower.inverted = true

                configure(flywheelFollower, flywheelMaster) {
                    limitSupplyCurrent(50.amps)
                    config_kP(0, kP)
                    configVelocityMeasurementPeriod(VelocityMeasPeriod.Period_5Ms)
                    configVelocityMeasurementWindow(32)

                    enableVoltageCompensation(true)
                    configVoltageCompSaturation(12.0)

                    setNeutralMode(NeutralMode.Brake)
                }
            }
        }

        object Hood {
            const val servoChannel = 0
            const val potChannel = 0

            const val kP = 2.4
            const val kI = 0.0
            const val kD = 0.275

            const val positionTolerance = 0.1

            const val minSetpoint = 12.5
            const val maxSetpoint = 24.5

            val servo = Servo(servoChannel)
            val potentiometer = AnalogPotentiometer(potChannel, 40.0, 0.0) // Sensor units in cm
        }

        object Turret {
            val turretMotor = CANSparkMax(1, CANSparkMaxLowLevel.MotorType.kBrushless)
            val turretEncoder = turretMotor.encoder
            val turretPID = turretMotor.pidController

            const val babyNeoCPR = 42
            const val turretReduction = (1.0 / 50.0) * (16.0 / 150.0)

            const val turretEncoderScaling = 1.30 / 5.0

            // Doesn't work for some reason???
            // converts encoder counts to degrees
            const val turretEncoderValueToDegrees = (1 / babyNeoCPR) * turretReduction * (1.0 / 360)

            // converts encoder RPM to degrees per second
            const val turretEncoderVelocityToDegPerSecond = turretReduction * (1.0 / 360) * 60.0

            const val turretMaxAngle = 70.0
            const val turretMinAngle = -185.0

            const val kP = 0.0001
            const val kD = 0.0003
            const val kI = 0.0

            const val maxVel = 20000.0
            const val maxAccel = 25000.0
            const val maxAccelVision = 1500.0

            val visionLatency = 100.milliseconds

            val visionScaling = 1.0
            val horizontalScaling = 1.0

            val tolerance = 2.0.degrees

            init {
                turretMotor.setSmartCurrentLimit(15, 40)

                turretEncoder.positionConversionFactor = turretEncoderValueToDegrees // Doesn't actually work...?
                turretEncoder.velocityConversionFactor = turretEncoderVelocityToDegPerSecond

                turretEncoder.measurementPeriod = 20

                turretEncoder.position = 0.0

                turretPID.p = kP
                turretPID.i = kI
                turretPID.d = kD
                turretPID.setSmartMotionMaxVelocity(maxVel, 0)
                turretPID.setSmartMotionMaxAccel(maxAccel, 0)
            }
        }

        object Vision {
            const val ledDioPin = 0
            const val brightness = 1.0
        }

    }

    object Climber {
        val climberMotor = TalonFX(5)

        const val kP = 0.03

        const val startHoldPosition = -7000.0
        const val extendedPosition = 307000.0
        const val positionTolerance = 6000

        init {
            climberMotor.limitSupplyCurrent(60.amps)
            climberMotor.setNeutralMode(NeutralMode.Brake)
            climberMotor.config_kP(0, kP)
            climberMotor.selectedSensorPosition = 0
        }
    }

}

