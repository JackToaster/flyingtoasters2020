package frc.robot.autonomous

import edu.wpi.first.wpilibj.geometry.Translation2d
import edu.wpi.first.wpilibj2.command.Command
import edu.wpi.first.wpilibj2.command.CommandBase
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup
import frc.robot.subsystems.drive.Drive

open class Autonomous(vararg commands: Command, val startingPosition: Translation2d = Translation2d()) : SequentialCommandGroup(*commands) {
    override fun initialize() {
        Drive.resetOdometry(startingPosition)
        super.initialize()
    }
}