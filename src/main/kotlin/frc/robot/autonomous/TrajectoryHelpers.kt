package frc.robot.autonomous

import edu.wpi.first.wpilibj.controller.RamseteController
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward
import edu.wpi.first.wpilibj.geometry.Pose2d
import edu.wpi.first.wpilibj.geometry.Rotation2d
import edu.wpi.first.wpilibj.geometry.Transform2d
import edu.wpi.first.wpilibj.geometry.Translation2d
import edu.wpi.first.wpilibj.trajectory.Trajectory
import edu.wpi.first.wpilibj.trajectory.TrajectoryConfig
import edu.wpi.first.wpilibj.trajectory.TrajectoryGenerator
import edu.wpi.first.wpilibj.trajectory.constraint.DifferentialDriveVoltageConstraint
import edu.wpi.first.wpilibj2.command.RamseteCommand
import frc.robot.Config
import frc.robot.subsystems.drive.Drive
import java.util.function.BiConsumer
import java.util.function.Supplier
import kotlin.math.PI

enum class Speeds(val velocity: Double, val acceleration: Double){
    Safe(0.5, 0.5),
    Slow(1.0, 1.5),
    MedSlow(2.0, 2.5),
    Med(3.0, 2.5),
    Fast(4.0, 3.0),
    Ludicrous(5.0, 9.0), // Actually faster than the robot can go,
                                         // so make sure to use voltage constraints!
}

// Create a voltage constraint to ensure we don't accelerate too fast
private val autoVoltageConstraint = DifferentialDriveVoltageConstraint(
        SimpleMotorFeedforward(Config.Drive.Characterization.kS,
                Config.Drive.Characterization.kV,
                Config.Drive.Characterization.kA),
        Config.Drive.Characterization.driveKinematics,
        10.0)

// Create config for trajectory
fun trajectoryConfig(speed: Speeds = Config.Trajectories.defaultSpeed, backwards: Boolean = false): TrajectoryConfig =
        if(Config.Trajectories.safetyMode){
            TrajectoryConfig(Speeds.Safe.velocity, Speeds.Safe.acceleration)
        }else{
            TrajectoryConfig(speed.velocity, speed.acceleration)
        }
        .setReversed(backwards)
        .setKinematics(Config.Drive.Characterization.driveKinematics) // Add kinematics to ensure max speed is actually obeyed
        .addConstraint(autoVoltageConstraint) // Apply the voltage constraint

open class FollowTrajectory(val trajectory: Trajectory): ToastyRamseteCommand(
        trajectory,
        Supplier { Drive.getPose() ?: Pose2d() },
        RamseteController(Config.Drive.ramseteB, Config.Drive.ramseteZeta),
        Config.Drive.Characterization.driveKinematics,
        BiConsumer<Double, Double> { p0, p1 -> Drive.setVelocities(p0, p1)},
        Drive
)

fun waypoint(x: Double, y: Double, degrees: Double) = Pose2d(x, y, Rotation2d(degrees * PI / 180.0))

fun transform(x: Double, y: Double, degrees: Double) = Transform2d(Translation2d(x, y), Rotation2d.fromDegrees(degrees))


fun point(x: Double, y: Double) = Translation2d(x, y)

fun trajectory(start: Pose2d,
               vararg interiorWaypoints: Translation2d,
               end: Pose2d,
               config: TrajectoryConfig = trajectoryConfig(Config.Trajectories.defaultSpeed)) =
        TrajectoryGenerator.generateTrajectory(start, interiorWaypoints.toList(), end, config)

fun trajectory(vararg waypoints: Pose2d,
               config: TrajectoryConfig = trajectoryConfig(Config.Trajectories.defaultSpeed)) =
        TrajectoryGenerator.generateTrajectory(waypoints.toList(), config)
