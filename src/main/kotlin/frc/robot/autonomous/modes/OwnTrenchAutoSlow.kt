package frc.robot.autonomous.modes

import edu.wpi.first.wpilibj2.command.ParallelRaceGroup
import frc.robot.autonomous.*
import frc.robot.subsystems.IntakeCommand
import frc.robot.subsystems.drive.StopDrive
import org.ghrobotics.lib.mathematics.units.feet
import org.ghrobotics.lib.mathematics.units.inMeters

private val underTrench = waypoint(20.feet.inMeters(), 5.5.feet.inMeters(), 0.0)

private val driveUnderTrench = trajectory(
        waypoint(0.0, 0.0, 0.0),
        waypoint(10.feet.inMeters(), 5.5.feet.inMeters(), 0.0),
        underTrench
)

private val outFromTrench = waypoint(16.feet.inMeters(), 5.5.feet.inMeters(), 0.0)

private val jTurnPivot = waypoint(7.feet.inMeters(), 5.5.feet.inMeters(), -45.0)

private val driveOutFromTrench = trajectory(
        underTrench,
        outFromTrench,
        jTurnPivot,
        config = trajectoryConfig(speed = Speeds.Fast, backwards = true)
)


private val jTurnEnd = trajectory(
        jTurnPivot,
        waypoint(10.feet.inMeters(), 2.feet.inMeters(), -70.0),
        config = trajectoryConfig(speed = Speeds.Fast)
)

class OwnTrenchAutoSlow: Autonomous(
        AutoShootCoommand(3, feedTimeout = 2.0),
        ParallelRaceGroup(
                FollowTrajectory(driveUnderTrench),
                IntakeCommand()
        ),
        FollowTrajectory(driveOutFromTrench),
        AutoShootCoommand(5),
        ParallelRaceGroup(
                FollowTrajectory(jTurnEnd),
                IntakeCommand()
        ),
        AutoShootCoommand(2),
        StopDrive()
)