package frc.robot.autonomous.modes

import frc.robot.autonomous.*
import org.ghrobotics.lib.mathematics.units.inMeters
import org.ghrobotics.lib.mathematics.units.inches

val pushPartnerPath = trajectory(
        waypoint(0.0,0.0,0.0),
        waypoint(-7.inches.inMeters(), 0.0, 0.0),
        config = trajectoryConfig(Speeds.MedSlow, backwards = true)
)

class PushPartnerCommand : FollowTrajectory(pushPartnerPath)
