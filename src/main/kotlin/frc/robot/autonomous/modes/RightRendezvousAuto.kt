package frc.robot.autonomous.modes

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup
import edu.wpi.first.wpilibj2.command.ParallelDeadlineGroup
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup
import frc.lib.commands.Wait
import frc.robot.autonomous.*
import frc.robot.subsystems.IntakeCommand
import frc.robot.subsystems.IntakeRetractCommand
import org.ghrobotics.lib.mathematics.units.seconds

class RightRendezvousAuto : Autonomous(
        ParallelDeadlineGroup(// Drive while intaking first ball
                FollowTrajectory(centerStartToRendezvousBalls),
                IntakeCommand(),
                AimNoVisionCommand(bothRightRendezvousBalls)
        ),
        AutoShootCoommand(5, feedTimeout = 10.0), //pew pew pew pew pew
        IntakeRetractCommand(),
        startingPosition = centerStart
)