package frc.robot.autonomous.modes

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup
import edu.wpi.first.wpilibj2.command.ParallelDeadlineGroup
import frc.robot.autonomous.*
import frc.robot.subsystems.IntakeCommand
import frc.robot.subsystems.IntakeRetractCommand

class OpponentTrenchAuto: Autonomous(
        ParallelDeadlineGroup(
                FollowTrajectory(leftTrenchGetTwoBalls),
                IntakeCommand()
        ),
        IntakeRetractCommand(),
        ParallelDeadlineGroup(
                FollowTrajectory(rightTrenchBackUp),
                AimNoVisionCommand(leftShootSpot)
        ),
        AutoShootCoommand(5),
        startingPosition = leftTrenchStart.translation
)