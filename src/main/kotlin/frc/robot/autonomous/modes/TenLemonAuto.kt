package frc.robot.autonomous.modes

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup
import edu.wpi.first.wpilibj2.command.ParallelDeadlineGroup
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup
import frc.lib.commands.Wait
import frc.robot.autonomous.*
import frc.robot.subsystems.IntakeCommand
import frc.robot.subsystems.IntakeRetractCommand
import org.ghrobotics.lib.mathematics.units.seconds

class TenLemonAuto: Autonomous(
        ParallelDeadlineGroup(// Drive while intaking first ball
                FollowTrajectory(centerStartToRendezvousBalls),
                IntakeCommand(),
                AimNoVisionCommand(rightTrenchStart)
        ),
        // Back up while aiming
        ParallelDeadlineGroup(
                FollowTrajectory(rightRendezvousToTrenchStart),
                AimNoVisionCommand(rightTrenchStart10)
        ),
        AutoShootCoommand(2, feedTimeout = 999.0), // pew pew
        ParallelDeadlineGroup(
                AutoShootCoommand(3, feedTimeout = 999.0), // pew pew pew
                IntakeRetractCommand()
        ),
        // Get five more!
        ParallelDeadlineGroup(
                FollowTrajectory(rightStartGetFiveBalls),
                IntakeCommand()
        ),
        // back up and start aiming
        ParallelCommandGroup(
                FollowTrajectory(rightTrenchBackUp10),

                // Wait 'till we're out of the trench and then start aiming
                SequentialCommandGroup(
                        Wait(1.0.seconds),
                        AimNoVisionCommand(rightTrenchCloser)
                )
        ),
        AutoShootCoommand(5, feedTimeout = 10.0), //pew pew pew pew pew
        // Retract the intake at the end
        IntakeRetractCommand(),
        startingPosition = centerStart
)