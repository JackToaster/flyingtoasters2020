package frc.robot.autonomous.modes

import edu.wpi.first.wpilibj.geometry.Translation2d
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup
import edu.wpi.first.wpilibj2.command.ParallelDeadlineGroup
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup
import frc.lib.commands.Wait
import frc.robot.autonomous.*
import frc.robot.subsystems.IntakeCommand
import org.ghrobotics.lib.mathematics.units.SIUnit
import org.ghrobotics.lib.mathematics.units.feet
import org.ghrobotics.lib.mathematics.units.inMeters
import org.ghrobotics.lib.mathematics.units.seconds

class ShootOnlyAuto(waitTime: Double): Autonomous(
        ParallelDeadlineGroup(
                ParallelCommandGroup(
                        FollowTrajectory(driveOffLine),
                        Wait(waitTime.seconds)
                ),
                // Aim without vision to where the target should be
                AimNoVisionCommand(waypoint(13.feet.inMeters(), 0.0, 0.0))
        ),
        AutoShootCoommand(3),

        startingPosition = centerStart
)
