package frc.robot.autonomous.modes

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup
import edu.wpi.first.wpilibj2.command.ParallelDeadlineGroup
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup
import frc.lib.commands.Wait
import frc.robot.autonomous.*
import frc.robot.subsystems.IntakeCommand
import frc.robot.subsystems.IntakeRetractCommand
import org.ghrobotics.lib.mathematics.units.feet
import org.ghrobotics.lib.mathematics.units.inMeters
import org.ghrobotics.lib.mathematics.units.milliseconds
import org.ghrobotics.lib.mathematics.units.seconds

class OwnTrenchAuto: Autonomous(
        AutoShootCoommand(1, feedTimeout = 999.0), // MUST shoot at least 1 to pick up 3
        AutoShootCoommand(2),
        ParallelDeadlineGroup(
                FollowTrajectory(rightStartGetThreeBalls),
                IntakeCommand(),
                // Aim without vision to where the target should be
                AimNoVisionCommand(rightTrenchCloser)
        ),
        ParallelCommandGroup(
                IntakeRetractCommand(),
                FollowTrajectory(rightTrenchBackUp)
        ),
        AutoShootCoommand(3),
        startingPosition = rightTrenchStart.translation
)

class OwnTrenchAuto8: Autonomous(
        AutoShootCoommand(3, feedTimeout = 999.0), // Must shoot all 3 to pick up 5
        ParallelDeadlineGroup(
                FollowTrajectory(rightStartGetFiveBalls),
                IntakeCommand()
        ),
        ParallelCommandGroup(
                FollowTrajectory(rightTrenchBackUpFive),

                // Wait a moment then retract the intake
                // Actually don't because that jams the intake
//                SequentialCommandGroup(
//                        Wait(200.milliseconds),
//                        IntakeRetractCommand()
//                ),

                // Wait 'till we're out of the trench and then start aiming
                SequentialCommandGroup(
                        Wait(1.0.seconds),
                        AimNoVisionCommand(rightTrenchCloser)
                )
        ),
        ParallelDeadlineGroup(
                AutoShootCoommand(5, feedTimeout = 10.0)

                // Unjam the stuck balls in the corner (If they get stuck)
                // Not needed in new version where intake doesn't retract
//                SequentialCommandGroup(
//                        Wait(1.seconds),
//                        IntakeCommand(),
//                        Wait(0.5.seconds),
//                        IntakeRetractCommand()
//                )
        ),
        // Retract the intake if we finished shooting while unjamming the corner ball. Probably not necessary
        IntakeRetractCommand(),
        startingPosition = rightTrenchStart.translation
)