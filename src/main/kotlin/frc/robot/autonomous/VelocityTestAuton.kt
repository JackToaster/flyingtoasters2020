package frc.robot.autonomous

import edu.wpi.first.wpilibj2.command.CommandBase
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup
import frc.lib.commands.Wait
import frc.robot.subsystems.drive.Drive
import org.ghrobotics.lib.mathematics.units.seconds

class DriveVelocityCommand(val left: Double, val right: Double): CommandBase(){
    init {
        addRequirements(Drive)
    }
    override fun execute() {
        Drive.setVelocities(left, right)
    }

    override fun end(interrupted: Boolean) {
        Drive.setVelocities(0.0, 0.0)
    }
}

class VelocityTestAuton: ParallelRaceGroup (
        DriveVelocityCommand(0.5, 0.5),
        Wait(5.seconds)
)