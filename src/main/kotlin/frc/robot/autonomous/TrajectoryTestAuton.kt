package frc.robot.autonomous

import edu.wpi.first.wpilibj2.command.InstantCommand
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup
import frc.robot.subsystems.drive.Drive

// An example trajectory to follow.  All units in meters.
private val testTrajectory = trajectory(
        waypoint(0.0, 0.0, 0.0), // Start at the origin facing the +X direction
        point(2.0, 1.0), // Pass through these two interior waypoints, making an 's' curve path
        point(2.0, -1.0),
        end=waypoint(4.0, 0.0, 0.0),
        // End 3 meters straight ahead of where we started, facing forward
        config = trajectoryConfig(Speeds.Med)
)

// Run path following command, then stop at the end.
class TrajectoryTestAuton: SequentialCommandGroup(
        FollowTrajectory(testTrajectory),
        InstantCommand(Runnable {
            Drive.setVelocities(0.0, 0.0)
        }, Drive)
)