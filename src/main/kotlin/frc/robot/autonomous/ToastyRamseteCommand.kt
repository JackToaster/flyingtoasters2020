package frc.robot.autonomous

import edu.wpi.first.wpilibj.Timer
import edu.wpi.first.wpilibj.controller.PIDController
import edu.wpi.first.wpilibj.controller.RamseteController
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward
import edu.wpi.first.wpilibj.geometry.Pose2d
import edu.wpi.first.wpilibj.kinematics.ChassisSpeeds
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveKinematics
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard
import edu.wpi.first.wpilibj.trajectory.Trajectory
import edu.wpi.first.wpilibj.util.ErrorMessages
import edu.wpi.first.wpilibj2.command.CommandBase
import edu.wpi.first.wpilibj2.command.Subsystem
import java.util.function.BiConsumer
import java.util.function.Supplier

open class ToastyRamseteCommand : CommandBase {
    private val m_timer = Timer()
    private val m_usePID: Boolean
    private val m_trajectory: Trajectory
    private val m_pose: Supplier<Pose2d>
    private val m_follower: RamseteController
    private val m_feedforward: SimpleMotorFeedforward?
    private val m_kinematics: DifferentialDriveKinematics
    private val m_speeds: Supplier<DifferentialDriveWheelSpeeds>?
    private val m_leftController: PIDController?
    private val m_rightController: PIDController?
    private val m_output: BiConsumer<Double, Double>
    private var m_prevSpeeds: DifferentialDriveWheelSpeeds? = null
    private var m_prevTime = 0.0

    /**
     * Constructs a new RamseteCommand that, when executed, will follow the provided trajectory.
     * PID control and feedforward are handled internally, and outputs are scaled -12 to 12
     * representing units of volts.
     *
     *
     * Note: The controller will *not* set the outputVolts to zero upon completion of the path -
     * this
     * is left to the user, since it is not appropriate for paths with nonstationary endstates.
     *
     * @param trajectory      The trajectory to follow.
     * @param pose            A function that supplies the robot pose - use one of
     * the odometry classes to provide this.
     * @param controller      The RAMSETE controller used to follow the trajectory.
     * @param feedforward     The feedforward to use for the drive.
     * @param kinematics      The kinematics for the robot drivetrain.
     * @param wheelSpeeds     A function that supplies the speeds of the left and
     * right sides of the robot drive.
     * @param leftController  The PIDController for the left side of the robot drive.
     * @param rightController The PIDController for the right side of the robot drive.
     * @param outputVolts     A function that consumes the computed left and right
     * outputs (in volts) for the robot drive.
     * @param requirements    The subsystems to require.
     */
    constructor(trajectory: Trajectory,
                pose: Supplier<Pose2d>,
                controller: RamseteController,
                feedforward: SimpleMotorFeedforward?,
                kinematics: DifferentialDriveKinematics,
                wheelSpeeds: Supplier<DifferentialDriveWheelSpeeds>,
                leftController: PIDController,
                rightController: PIDController,
                outputVolts: BiConsumer<Double, Double>,
                vararg requirements: Subsystem?) {
        m_trajectory = ErrorMessages.requireNonNullParam(trajectory, "trajectory", "RamseteCommand")
        m_pose = ErrorMessages.requireNonNullParam(pose, "pose", "RamseteCommand")
        m_follower = ErrorMessages.requireNonNullParam(controller, "controller", "RamseteCommand")
        m_feedforward = feedforward
        m_kinematics = ErrorMessages.requireNonNullParam(kinematics, "kinematics", "RamseteCommand")
        m_speeds = ErrorMessages.requireNonNullParam(wheelSpeeds, "wheelSpeeds", "RamseteCommand")
        m_leftController = ErrorMessages.requireNonNullParam(leftController, "leftController", "RamseteCommand")
        m_rightController = ErrorMessages.requireNonNullParam(rightController, "rightController", "RamseteCommand")
        m_output = ErrorMessages.requireNonNullParam(outputVolts, "outputVolts", "RamseteCommand")
        m_usePID = true
        addRequirements(*requirements)
    }

    /**
     * Constructs a new RamseteCommand that, when executed, will follow the provided trajectory.
     * Performs no PID control and calculates no feedforwards; outputs are the raw wheel speeds
     * from the RAMSETE controller, and will need to be converted into a usable form by the user.
     *
     * @param trajectory            The trajectory to follow.
     * @param pose                  A function that supplies the robot pose - use one of
     * the odometry classes to provide this.
     * @param follower              The RAMSETE follower used to follow the trajectory.
     * @param kinematics            The kinematics for the robot drivetrain.
     * @param outputMetersPerSecond A function that consumes the computed left and right
     * wheel speeds.
     * @param requirements          The subsystems to require.
     */
    constructor(trajectory: Trajectory,
                pose: Supplier<Pose2d>,
                follower: RamseteController,
                kinematics: DifferentialDriveKinematics,
                outputMetersPerSecond: BiConsumer<Double, Double>,
                vararg requirements: Subsystem?) {
        m_trajectory = ErrorMessages.requireNonNullParam(trajectory, "trajectory", "RamseteCommand")
        m_pose = ErrorMessages.requireNonNullParam(pose, "pose", "RamseteCommand")
        m_follower = ErrorMessages.requireNonNullParam(follower, "follower", "RamseteCommand")
        m_kinematics = ErrorMessages.requireNonNullParam(kinematics, "kinematics", "RamseteCommand")
        m_output = ErrorMessages.requireNonNullParam(outputMetersPerSecond, "output", "RamseteCommand")
        m_feedforward = null
        m_speeds = null
        m_leftController = null
        m_rightController = null
        m_usePID = false
        addRequirements(*requirements)
    }

    override fun initialize() {
        m_prevTime = 0.0
        val initialState = m_trajectory.sample(0.0)
        m_prevSpeeds = m_kinematics.toWheelSpeeds(
                ChassisSpeeds(initialState.velocityMetersPerSecond,
                        0.0, initialState.curvatureRadPerMeter
                        * initialState.velocityMetersPerSecond))
        m_timer.reset()
        m_timer.start()
        if (m_usePID) {
            m_leftController!!.reset()
            m_rightController!!.reset()
        }
    }

    override fun execute() {
        val curTime = m_timer.get()
        val dt = curTime - m_prevTime
        val targetPosition = m_trajectory.sample(curTime)
        SmartDashboard.putNumber("Target X", targetPosition.poseMeters.translation.x)
        SmartDashboard.putNumber("Target Y", targetPosition.poseMeters.translation.y)
        SmartDashboard.putNumber("Target Angle", targetPosition.poseMeters.rotation.degrees)
        val targetWheelSpeeds = m_kinematics.toWheelSpeeds(
                m_follower.calculate(m_pose.get(), targetPosition))
        val leftSpeedSetpoint = targetWheelSpeeds.leftMetersPerSecond
        val rightSpeedSetpoint = targetWheelSpeeds.rightMetersPerSecond
        val leftOutput: Double
        val rightOutput: Double
        if (m_usePID) {
            val leftFeedforward = m_feedforward!!.calculate(leftSpeedSetpoint,
                    (leftSpeedSetpoint - m_prevSpeeds!!.leftMetersPerSecond) / dt)
            val rightFeedforward = m_feedforward.calculate(rightSpeedSetpoint,
                    (rightSpeedSetpoint - m_prevSpeeds!!.rightMetersPerSecond) / dt)
            leftOutput = (leftFeedforward
                    + m_leftController!!.calculate(m_speeds!!.get().leftMetersPerSecond,
                    leftSpeedSetpoint))
            rightOutput = (rightFeedforward
                    + m_rightController!!.calculate(m_speeds.get().rightMetersPerSecond,
                    rightSpeedSetpoint))
        } else {
            leftOutput = leftSpeedSetpoint
            rightOutput = rightSpeedSetpoint
        }
        m_output.accept(leftOutput, rightOutput)
        m_prevTime = curTime
        m_prevSpeeds = targetWheelSpeeds
    }

    override fun end(interrupted: Boolean) {
        m_timer.stop()
    }

    override fun isFinished(): Boolean {
        return m_timer.hasPeriodPassed(m_trajectory.totalTimeSeconds)
    }
}