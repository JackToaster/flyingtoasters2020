package frc.robot.autonomous

import edu.wpi.first.wpilibj.geometry.Pose2d
import edu.wpi.first.wpilibj.geometry.Rotation2d
import edu.wpi.first.wpilibj.geometry.Transform2d
import edu.wpi.first.wpilibj.geometry.Translation2d
import org.ghrobotics.lib.mathematics.units.feet
import org.ghrobotics.lib.mathematics.units.inMeters

val centerStart = Translation2d(10.feet.inMeters(), 0.0)
val centerStartPose = Pose2d(centerStart, Rotation2d())


val rightTrenchStart = waypoint(10.feet.inMeters(), 5.feet.inMeters(), 0.0)
val leftTrenchStart = waypoint(10.feet.inMeters(), -16.5.feet.inMeters(), 0.0)

val driveOffLine = trajectory(
        centerStartPose,
        waypoint(13.feet.inMeters(), 0.0, 0.0),
        config = trajectoryConfig(Speeds.Slow)
        )

val farRightRendezvousBall = waypoint(5.45, -0.10, -60.0)

val backFromRightRendezvousBall = farRightRendezvousBall + Transform2d(
        Translation2d(-0.5, 0.0),
        Rotation2d.fromDegrees(0.0)
)

val closeRightRendezvousBall = farRightRendezvousBall + Transform2d(
        Translation2d(0.15, -0.27),
        Rotation2d.fromDegrees(-35.0)
)

val bothRightRendezvousBalls = farRightRendezvousBall + transform(0.19, -0.40, 0.0)

val rightTrenchStart10 = rightTrenchStart + transform(0.0, -0.3, 0.0)

val rightRendezvousToTrenchStart = trajectory(
        bothRightRendezvousBalls,
        rightTrenchStart10,
        config = trajectoryConfig(Speeds.Fast, backwards = true)
)

val centerStartToFarRightRendezvousBall = trajectory(
        centerStartPose,
        farRightRendezvousBall,
        config = trajectoryConfig(Speeds.Med)
)

val backUpFromRightRendezvousBall = trajectory(
        farRightRendezvousBall,
        backFromRightRendezvousBall,
        config = trajectoryConfig(Speeds.Fast, backwards = true)
)

val toCloseRightRendezvousBall = trajectory(
        backFromRightRendezvousBall,
        closeRightRendezvousBall,
        config = trajectoryConfig(Speeds.Fast)
)

val centerStartToRendezvousBalls = trajectory(
        centerStartPose,
        bothRightRendezvousBalls,
        config = trajectoryConfig(Speeds.Fast)
)

val rightTrenchThreeBalls = rightTrenchStart + transform(12.5.feet.inMeters(), 0.0, 0.0)

val rightTrenchFiveBalls = rightTrenchStart + transform(18.5.feet.inMeters(), 0.0, 0.0)

val rightTrenchCloser = rightTrenchStart + transform(5.feet.inMeters(), 0.0, 0.0)

val rightStartGetThreeBalls = trajectory(
        rightTrenchStart,
        rightTrenchThreeBalls,
        config = trajectoryConfig(Speeds.MedSlow)
        )

val rightTrenchFiveBalls10 = rightTrenchStart10 + transform(16.5.feet.inMeters(), 0.0, 0.0)

val rightStartGetFiveBalls = trajectory(
        rightTrenchStart10,
        rightTrenchFiveBalls10,
        config = trajectoryConfig(Speeds.MedSlow)
)

val rightTrenchBackUp = trajectory(
        rightTrenchThreeBalls,
        rightTrenchCloser,
        config = trajectoryConfig(Speeds.Fast, backwards = true)
)

val rightTrenchBackUp10 = trajectory(
        rightTrenchFiveBalls10,
        rightTrenchFiveBalls10 + transform(-10.0.feet.inMeters(), 0.0, 0.0),
        config = trajectoryConfig(Speeds.Fast, backwards = true)
)

val rightTrenchBackUpFive = trajectory(
        rightTrenchFiveBalls,
        rightTrenchCloser,
        config = trajectoryConfig(Speeds.Fast, backwards = true)
)

val leftTrenchTwoBalls = leftTrenchStart + transform(7.5.feet.inMeters(), 0.0, 0.0)

val leftShootSpot = centerStartPose + transform(-2.0, 0.0, -90.0)

val leftTrenchGetTwoBalls = trajectory(
        leftTrenchStart, leftTrenchTwoBalls,
        config = trajectoryConfig(Speeds.MedSlow)
        )

val leftTrenchRunAway = trajectory(
        leftTrenchTwoBalls, leftShootSpot,
        config = trajectoryConfig(Speeds.Fast, backwards = true)
)