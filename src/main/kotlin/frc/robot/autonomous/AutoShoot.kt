package frc.robot.autonomous

import edu.wpi.first.wpilibj.geometry.Pose2d
import edu.wpi.first.wpilibj2.command.*
import frc.lib.commands.BlockCommand
import frc.lib.commands.RepeatedCommand
import frc.lib.commands.Wait
import frc.robot.Config
import frc.robot.subsystems.*
import frc.robot.subsystems.launcher.*
import org.ghrobotics.lib.mathematics.units.seconds

class AimCommand() : SequentialCommandGroup(
        LedOnCommand(),
        ParallelDeadlineGroup(
                ParallelCommandGroup(// Deadline is spinning up, moving hood, and aiming
                        FlywheelSpinUpCommand({ Vision.getSetpoint().flywheelVelocity }),
                        HoodCommand({ Vision.getSetpoint().hoodPosition }),
                        FieldRelativeTurretCommand({ Vision.getSetpoint().turretAngle }),
                        WaitForTargetLock()
                ),
                PrepBallToShoot()
        )
)

class AimAndHoldCommand() : ParallelCommandGroup(
        SequentialCommandGroup(
                ParallelCommandGroup(// Deadline is spinning up, moving hood, and aiming
                        FlywheelSpinUpCommand({ Vision.getSetpoint().flywheelVelocity }),
                        HoodCommand({ Vision.getSetpoint().hoodPosition }),
                        FieldRelativeTurretCommand({ Vision.getSetpoint().turretAngle }),
                        WaitForTargetLock()
                ),
                ParallelCommandGroup( // Hold flywheel and turret and hood
                        FlywheelCommand({ Vision.getSetpoint().flywheelVelocity }),
                        FieldRelativeHoldCommand({ Vision.getSetpoint().turretAngle }),
                        HoodTrackCommand({ Vision.getSetpoint().hoodPosition })
                )
        ),
        PrepBallToShoot()
)

fun AimNoVisionCommand(aimPosition: Pose2d): Command {
    val target = getSelectedTargetingData(Pose2d(), aimPosition)
    val setpoint = getSetpointFromTargetingData(target) ?: return BlockCommand({})
    val robotRelativeTurretAngle = Turret.fieldRelativeToRobotRelativeAngle(setpoint.turretAngle, aimPosition.rotation)

    return ParallelCommandGroup(
            FlywheelSpinUpCommand({ setpoint.flywheelVelocity }),
            HoodCommand({ setpoint.hoodPosition }),
            TurretCommand(robotRelativeTurretAngle),
            PrepBallToShoot()
    )
}

class ShootOnlyCommand(val number: Int, val feedTimeout: Double = 5.0) : ParallelDeadlineGroup(
                SequentialCommandGroup(
                        Wait(0.5.seconds),
                        RepeatedCommand(FeedOneBall(), number).withTimeout(feedTimeout)
                ),
                FlywheelCommand({ Vision.getSetpoint().flywheelVelocity }),
                FieldRelativeHoldCommand({ Vision.getSetpoint().turretAngle }),
                HoodTrackCommand({ Vision.getSetpoint().hoodPosition})
        ) {
    override fun end(interrupted: Boolean) {
        super.end(interrupted)
        Flywheel.flywheelOff()
        Hood.setSetpoint(Config.Launcher.Hood.minSetpoint)

        // Don't really need to reset turret in auton, that's just wasting time
//        Turret.turretHome()
        VisionLEDs.set(false)
    }
}

class AutoShootCoommand(val number: Int, val feedTimeout: Double = 5.0, val ready: Boolean = false) : SequentialCommandGroup(
        AimCommand(),
        ParallelDeadlineGroup(
                SequentialCommandGroup(
                        Wait(0.3.seconds),
                        RepeatedCommand(
                                SequentialCommandGroup(
                                        FeedOneBall(),
                                        ParallelDeadlineGroup(
                                            WaitForSpinUpCommand({ Vision.getSetpoint().flywheelVelocity })
                                            )
//                                            GetBall() // For some reason this means it can't count the balls.
                                        ),
                                number
                        ).withTimeout(feedTimeout)
                ),
                FlywheelCommand({ Vision.getSetpoint().flywheelVelocity }),
                FieldRelativeHoldCommand({ Vision.getSetpoint().turretAngle }),
                HoodTrackCommand({ Vision.getSetpoint().hoodPosition})
        )
) {

    override fun end(interrupted: Boolean) {
        super.end(interrupted)
        Flywheel.flywheelOff()
        Hood.setSetpoint(Config.Launcher.Hood.minSetpoint)
//        BallFeeder.setSpeed(0.0)
        // Don't really need to reset turret in auton, that's just wasting time
//        Turret.turretHome()
        VisionLEDs.set(false)
    }
}