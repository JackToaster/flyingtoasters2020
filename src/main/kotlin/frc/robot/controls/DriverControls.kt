package frc.robot.controls

import edu.wpi.first.wpilibj.geometry.Rotation2d
import frc.lib.hid.GuitarController
import frc.lib.hid.PS4Controller
import frc.robot.Config.Launcher.Flywheel.shootVelocity
import frc.robot.autonomous.AutoShootCoommand
import frc.robot.subsystems.*
import frc.robot.subsystems.launcher.*
import org.ghrobotics.lib.mathematics.units.seconds

object ControlMapping {
    // Grilled Cheese drive parameters
    const val kArcadeRotationGain = 0.6 // How quickly the robot turns while stationary
    const val kArcadeThreshold = 0.3 // Above this speed, curvature drive is used.
    const val kCurvatureGain = 0.7 // How quickly the robot turns in curvature drive mode
    const val kSpeedGain = 1.0 // Maximum motor power
    const val kSpeedExponent = 1.5 // Exponent for speed control input
    const val kTurnExponent = 2.0 // Exponent for turn control input

    val drivePower = { PS4.getRawAxis(PS4Controller.Axis.LeftY) }
    val driveTurn = {-PS4.getRawAxis(PS4Controller.Axis.RightX) }

    val climberPower = { (PS4.getRawAxis(PS4Controller.Axis.RightTrigger) + 1.0) / 2.0 }

    init {
        // Touchpad controls the vision LEDs for testing
        val touchpad = PS4.joystickButton(PS4Controller.Button.Touchpad)

        touchpad.whenPressed(LedOnCommand())
                .whenReleased(LedOffCommand())

        // Just for fun, or maybe for distracted human players.
        PS4.joystickButton(PS4Controller.Button.Options)
                .whenHeld(LedStrobeCommand(0.1.seconds))

//        driverController.start
//                .whenHeld(PlaySongCommand()) // RIP Music player

        // Left trigger intakes. Also gets one ball in the tower.
        PS4.joystickButton(PS4Controller.Button.TriggerLeft)
                // Removed because it interferes with shooting when using the intake to unjam balls
//                .whenPressed(PrepBallToShoot().withTimeout(5.0))
                .whenHeld(IntakeCommand())
                .whenReleased(IntakeRetractCommand())

        // X gets a ball into the tower
        PS4.joystickButton(PS4Controller.Button.X)
                .whenPressed(GetBall().withTimeout(4.20))

        Guitar.joystickButton(GuitarController.Button.Green)
                .whenPressed(GetBall().withTimeout(4.20))

        // Left bumper unjams intake by spinning it backwards
        PS4.joystickButton(PS4Controller.Button.BumperLeft)
                .whenHeld(IntakeEjectCommand())

        // Hold right bumper to climb
        PS4.joystickButton(PS4Controller.Button.BumperRight)
                .whenHeld(TeleopClimbCommand())

        // Hopefully temporary
        // Reset the intake encoder position
        PS4.joystickButton(PS4Controller.Button.StickLeft)
                .whenPressed(IntakeResetCommand())

        // Go into reset mode after pressing both sticks down
        PS4.joystickButton(PS4Controller.Button.StickLeft).and(
                PS4.joystickButton(PS4Controller.Button.StickRight)
        ).whenActive(TeleopClimberRetractCommand())

        // Whammy bar spins up but doesn't move the hood so we can spin up in the trench.
        Guitar.joystickAxisButton(GuitarController.Axis.Whammy, 0.5, false)
                .whenHeld(AimAndSpinUpCommand())
                .whenReleased(FlywheelStopCommand())

        // Orange does target zone shot
        Guitar.joystickButton(GuitarController.Button.Orange)
                .whenHeld(shootTargetZoneCommand())

        // Fine, I'll give you your "shoot while stationary" button.
        Guitar.joystickButton(GuitarController.Button.Blue)
                .whenHeld(ShootStationaryCommand())

        // Strum to shoot
        Guitar.povButton(180)
                .whenHeld(ShootCommand())
        Guitar.povButton(0)
                .whenHeld(ShootCommand())

        // Reset the turret to forward
        PS4.joystickButton(PS4Controller.Button.Share).whenPressed (TurretHoldCommand(Rotation2d()))

        // Below commands were for testing
//        Guitar.joystickButton(GuitarController.Button.Blue).whenPressed(
//                FieldRelativeHoldCommand({Rotation2d.fromDegrees(0.0)})
//        )
//        Guitar.joystickButton(GuitarController.Button.Yellow).whenPressed(HoodCommand {18.5})
//
//        Guitar.joystickButton(GuitarController.Button.Red).whenPressed(AutoShootCoommand(3))
    }
}

object PS4 : PS4Controller(0)

object Guitar: GuitarController(1)
