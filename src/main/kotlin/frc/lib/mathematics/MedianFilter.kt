package frc.lib.mathematics

import edu.wpi.first.wpiutil.CircularBuffer
import java.util.*
import kotlin.math.abs

// Adapted from WPILib's MedianFilter
class MedianFilter(val size: Int) {
    private val valueBuffer: CircularBuffer = CircularBuffer(size)
    private val orderedValues: MutableList<Double> = ArrayList(size)

    // Adds one value to the filter, and pops one value if there are too many
    fun push(next: Double) {
        // Find insertion point for next value
        var index = Collections.binarySearch(orderedValues, next)
        // Deal with binarySearch behavior for element not found
        if (index < 0) {
            index = abs(index + 1)
        }
        // Place value at proper insertion point
        orderedValues.add(index, next)

        val curSize = orderedValues.size
        // If buffer is at max size, pop element off of end of circular buffer
        // and remove from ordered list
        if (curSize > size) {
            orderedValues.remove(valueBuffer.removeLast())
        }
        // Add next value to circular buffer
        valueBuffer.addFirst(next)
    }

    fun pop() {
        orderedValues.remove(valueBuffer.removeLast())
    }

    fun calculate(): Double? {
        val curSize = orderedValues.size

        // Cannot calculate with an empty filter
        if(curSize == 0){
            return null
        }

        return if (curSize % 2 == 1) { // If size is odd, return middle element of sorted list
            orderedValues[curSize / 2]
        } else { // If size is even, return average of middle elements
            (orderedValues[curSize / 2 - 1] + orderedValues[curSize / 2]) / 2.0
        }
    }


    /**
     * Resets the filter, clearing the window of all elements.
     */
    fun reset() {
        orderedValues.clear()
        valueBuffer.clear()
    }
}