package frc.lib.mathematics

class ComplementaryFilter(val absolutePercent: Double, initialPosition: Double){
    var lastRelativeMeasurement: Double = initialPosition
    var lastOutput: Double = initialPosition

    fun calculate(absoluteMeasurement: Double, relativeMeasurement: Double): Double{
        // Calculate how much the relative measurement has changed
        val relativeDelta = relativeMeasurement - lastRelativeMeasurement
        lastRelativeMeasurement = relativeMeasurement

        lastOutput = (1.0 - absolutePercent) * (lastOutput + relativeDelta) + absolutePercent * absoluteMeasurement

        return lastOutput
    }
}
