package frc.lib.mathematics

import java.util.*

interface Interpolatable<T> {
    abstract fun interpolate(other: T, alpha: Double): T
}

class InterpolatingLookupTable <V: Interpolatable<V>> : TreeMap<Double, V>() {
    override fun get(key: Double): V? {
        if(containsKey(key)){
            return super.get(key)
        }
        // Get the largest entry lower than key, or return the first entry if no entry is lower than key
        val lowerEntry = lowerEntry(key) ?: return firstEntry()?.value ?: return null

        // Get the smallest entry greater than key, or return the last entry if no entry is greater than key
        val higherEntry = higherEntry(key) ?: return lastEntry()?.value ?: return null

        val alpha = (key - lowerEntry.key) / (higherEntry.key - lowerEntry.key)
        return lowerEntry.value.interpolate(higherEntry.value, alpha)
    }
}

fun <V: Interpolatable<V>> interpolatingLookupFrom(vararg entries: Pair<Double, V>):
        InterpolatingLookupTable<V> {
    val table = InterpolatingLookupTable<V>()
    entries.forEach { table[it.first] = it.second }
    return table
}