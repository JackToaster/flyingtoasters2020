package frc.lib.hid

import edu.wpi.first.wpilibj.GenericHID
import edu.wpi.first.wpilibj.Joystick
import edu.wpi.first.wpilibj2.command.button.Button
import edu.wpi.first.wpilibj2.command.button.JoystickButton
import edu.wpi.first.wpilibj2.command.button.POVButton

class JoystickAxisButton(val joystick: GenericHID,
                         val axisNumber: Int,
                         val threshold: Double,
                         val inverted: Boolean = false): Button() {
    /**
     * Gets the value of the joystick button.
     *
     * @return The value of the joystick button
     */
    override fun get(): Boolean {
        val active = joystick.getRawAxis(axisNumber) > threshold

        return if(inverted) !active else active
    }
}

open class PS4Controller(port: Int): Joystick(port){
    enum class Button(val value: Int) {
        Square(1),
        X(2),
        Circle(3),
        Triangle(4),
        BumperLeft(5),
        BumperRight(6),
        TriggerLeft(7),
        TriggerRight(8),
        Share(9),
        Options(10),
        StickLeft(11),
        StickRight(12),
        Playstation(13),
        Touchpad(14)
    }

    enum class Axis(val value: Int) {
        LeftY(1),
        RightY(5),
        LeftX(0),
        RightX(2),
        LeftTrigger(3),
        RightTrigger(4)
    }

    fun getRawButton(button: Button) = getRawButton(button.value)
    fun getRawAxis(axis: Axis) = getRawAxis(axis.value)

    fun joystickButton(button: Button) = JoystickButton(this, button.value)
    fun joystickAxisButton(axis: Axis, threshold: Double, inverted: Boolean = false) =
            JoystickAxisButton(this, axis.value, threshold, inverted)
    fun povButton(angle: Int) = POVButton(this, angle)
}

open class GuitarController(port: Int): Joystick(port){
    enum class Button(val value: Int) {
        Green(1),
        Red(2),
        Blue(3),
        Yellow(4),
        Orange(5),
        Back(7),
        Start(8)
    }

    enum class Axis(val value: Int) {
        Whammy(4),
        //AccelX(1234), //TODO Check axes for tilt control on guitar
        //AccelY(5678),
        AccelZ(5)
    }

    fun getRawButton(button: Button) = getRawButton(button.value)
    fun getRawAxis(axis: Axis) = getRawAxis(axis.value)

    fun joystickButton(button: Button) = JoystickButton(this, button.value)
    fun joystickAxisButton(axis: Axis, threshold: Double, inverted: Boolean = false) =
            JoystickAxisButton(this, axis.value, threshold, inverted)
    fun povButton(angle: Int) = POVButton(this, angle)
}