package frc.lib.config

import com.ctre.phoenix.ErrorCode
import com.ctre.phoenix.motorcontrol.IMotorController
import com.ctre.phoenix.motorcontrol.IMotorControllerEnhanced
import com.ctre.phoenix.motorcontrol.SupplyCurrentLimitConfiguration
import com.ctre.phoenix.motorcontrol.can.BaseMotorController
import com.ctre.phoenix.motorcontrol.can.TalonFX
import org.ghrobotics.lib.mathematics.units.*
import org.ghrobotics.lib.mathematics.units.derived.acceleration
import org.ghrobotics.lib.mathematics.units.derived.velocity
import org.ghrobotics.lib.mathematics.units.nativeunit.*

infix fun BaseMotorController.follows(following: IMotorController): Unit = follow(following)

// Allows configuration of multiple motors, similar to with()
fun <T : IMotorController> configure(vararg motorControllers: T, block: T.() -> Unit) {
    motorControllers.forEach { it.block() }
}

////////////////////////////////////////////////////////
// Current Limiting

// Same idea as SupplyCurrentLimitConfiguration for TalonFX
data class CurrentLimitConfig(val maxCurrent: SIUnit<Ampere>,
                              val triggerCurrent: SIUnit<Ampere> = maxCurrent,
                              val triggerTime: SIUnit<Second> = 0.0.seconds) {
    fun toSupplyCurrentLimit(): SupplyCurrentLimitConfiguration =
            SupplyCurrentLimitConfiguration(true, maxCurrent.inAmps(), triggerCurrent.inAmps(), triggerTime.inMilliseconds())
}

// Stores the time/current needed to trigger a current limit
data class CurrentLimitTrigger(val triggerCurrent: SIUnit<Ampere>,
                               val triggerTime: SIUnit<Second> = 0.0.seconds)

// Creates current limit config from current and time
infix fun SIUnit<Ampere>.forTime(time: SIUnit<Second>) =
        CurrentLimitTrigger(this, time)


fun SIUnit<Ampere>.toCurrentLimitConfig() =
        CurrentLimitConfig(this)

fun SIUnit<Ampere>.toSupplyCurrentLimit() =
        toCurrentLimitConfig().toSupplyCurrentLimit()

// Set how the current limit is triggered
infix fun SIUnit<Ampere>.trigger(current: SIUnit<Ampere>) =
        CurrentLimitConfig(this, triggerCurrent = current)

infix fun SIUnit<Ampere>.trigger(trigger: CurrentLimitTrigger) =
        CurrentLimitConfig(this, trigger.triggerCurrent, trigger.triggerTime)


////////////////////////////////////////////////////////
// PIDs
data class PIDConfig(val P: Double = 0.0, val I: Double = 0.0, val D: Double = 0.0, val FF: Double = 0.0)
data class MotionMagicConfig(val pid: PIDConfig, val acceleration: Double, val velocity: Double)

////////////////////////////////////////////////////////
// Extensions for TalonFX
fun TalonFX.limitSupplyCurrent(limit: CurrentLimitConfig): ErrorCode =
        configSupplyCurrentLimit(limit.toSupplyCurrentLimit())

fun TalonFX.limitSupplyCurrent(limit: SIUnit<Ampere>): ErrorCode = limitSupplyCurrent(limit.toCurrentLimitConfig())

fun TalonFX.configPID(slot: Int = 0, p: Double = 0.0, i: Double = 0.0, d: Double = 0.0, ff: Double = 0.0){
    config_kP(slot, p)
    config_kI(slot, i)
    config_kD(slot, d)
    config_kF(slot, ff)
}

fun TalonFX.configMotionMagic(vel: SIUnit<NativeUnitVelocity>, accel: SIUnit<NativeUnitAcceleration>) {
    configMotionAcceleration(accel.inSTUPer100msPerSecond().toInt())
    configMotionCruiseVelocity(vel.inNativeUnitsPer100ms().toInt())
}

fun TalonFX.configMotionMagic(vel: Double, accel: Double) {
    configMotionAcceleration(accel.nativeUnits.acceleration.inSTUPer100msPerSecond().toInt())
    configMotionCruiseVelocity(vel.nativeUnits.velocity.inNativeUnitsPer100ms().toInt())
}
