package frc.lib.commands

import edu.wpi.first.wpilibj2.command.InstantCommand
import edu.wpi.first.wpilibj2.command.Subsystem
import edu.wpi.first.wpilibj2.command.WaitCommand
import org.ghrobotics.lib.mathematics.units.SIUnit
import org.ghrobotics.lib.mathematics.units.Second
import org.ghrobotics.lib.mathematics.units.inSeconds

open class BlockCommand(block: () -> Unit, vararg requirements: Subsystem = arrayOf()): InstantCommand(block, requirements)

class Wait(time: SIUnit<Second>): WaitCommand(time.inSeconds())
