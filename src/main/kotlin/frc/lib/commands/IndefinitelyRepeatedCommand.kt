package frc.lib.commands


import edu.wpi.first.wpilibj2.command.Command
import edu.wpi.first.wpilibj2.command.CommandGroupBase

class IndefinitelyRepeatedCommand(val command: Command): CommandGroupBase() {
    private var m_runWhenDisabled = true

    init {
        requireUngrouped(command)

        m_requirements.addAll(command.requirements)
        m_runWhenDisabled = command.runsWhenDisabled()
    }

    override fun addCommands(vararg commands: Command?) {} // Doesn't make sense in this situation

    override fun initialize() {
        command.initialize()
    }

    override fun execute() {
        command.execute()
        if (command.isFinished) {
            command.end(false)
            command.initialize()
        }
    }

    override fun end(interrupted: Boolean) {
        if (interrupted) {
            command.end(true)
        }
    }

    override fun isFinished(): Boolean {
        return false
    }

    override fun runsWhenDisabled(): Boolean {
        return m_runWhenDisabled
    }
}