package frc.lib.commands

import edu.wpi.first.wpilibj2.command.Command
import edu.wpi.first.wpilibj2.command.CommandGroupBase

class RepeatedCommand(val command: Command, val times: Int): CommandGroupBase() {
    private var m_runWhenDisabled = true
    private  var m_currentCommandIndex:Int = -1

    init {
        requireUngrouped(command)

        m_requirements.addAll(command.requirements)
        m_runWhenDisabled = command.runsWhenDisabled()
    }

    override fun addCommands(vararg commands: Command?) {} // Doesn't make sense in this situation

    override fun initialize() {
        m_currentCommandIndex = 0
        command.initialize()
    }

    override fun execute() {
        command.execute()
        if (command.isFinished) {
            command.end(false)
            m_currentCommandIndex++
            if (m_currentCommandIndex < times) {
                command.initialize()
            }
        }
    }

    override fun end(interrupted: Boolean) {
        if (interrupted &&  m_currentCommandIndex > -1 && m_currentCommandIndex < times) {
            command.end(true)
        }
        m_currentCommandIndex = -1
    }

    override fun isFinished(): Boolean {
        return m_currentCommandIndex == times
    }

    override fun runsWhenDisabled(): Boolean {
        return m_runWhenDisabled
    }
}